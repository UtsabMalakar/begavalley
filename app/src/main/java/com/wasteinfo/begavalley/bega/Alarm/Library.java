package com.wasteinfo.begavalley.bega.Alarm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Display;
import android.view.WindowManager;

public class Library {
	private Context context;
	private Typeface font_normal;
	private Typeface font_bold;
  

	public Library(Context context) {
		this.context = context;
		this.font_normal = Typeface.createFromAsset(this.context.getAssets(), "fonts/Exo_Light.TTF");
		this.font_bold = Typeface.createFromAsset(this.context.getAssets(), "fonts/Exo_Medium.TTF");
	}

	
	public Typeface getDefaultFont(String type){
		if(type.equalsIgnoreCase("regular"))
            return this.font_normal;
        else if(type.equalsIgnoreCase("bold"))
            return this.font_bold;
        else
            return this.font_normal;
        	
        
    }
	
	public Bitmap manualResizeByPercentWidthScreenDevice(Bitmap image,int percent_width) {
		WindowManager wm = (WindowManager) this.context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		float scale = ((float) (display.getWidth() * percent_width) / 100)
				/ image.getWidth();
		if (scale <= 1)
			return Bitmap.createScaledBitmap(image,(int) ((int) image.getWidth() * scale),(int) ((int) image.getHeight() * scale), true);
		else
			return image;
	}
	
	public float getWidthScreenDevice(int percent){
        WindowManager wm = (WindowManager) this.context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        return ((float)(display.getWidth()*percent)/100);
    }
	
	public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
    }

}
