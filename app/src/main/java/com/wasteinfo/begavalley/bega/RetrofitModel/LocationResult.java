package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationResult {


    @SerializedName("id")
    @Expose
    private long itemId;
    @SerializedName("unit_number")
    @Expose
    private String unitNumber;
    @SerializedName("house_number")
    @Expose
    private String houseNumber;
    @SerializedName("street_name")
    @Expose
    private String street;
    @SerializedName("street_typr")
    @Expose
    private String street_type;
    @SerializedName("suburb")
    @Expose
    private String suburb;
    @SerializedName("postcode")
    @Expose
    private String postcode;
    @SerializedName("collection_day")
    @Expose
    private String collectionDay;
    @SerializedName("atl_week")
    @Expose
    private String atlWeek;
    @SerializedName("property_p")
    @Expose
    private String propertyP;
    @SerializedName("pfi")
    @Expose
    private String pfi;
    @SerializedName("bin_type")
    @Expose
    private String binType;
    @SerializedName("date_time")
    @Expose
    private String dateTime;
    @SerializedName("current_week_type")
    @Expose
    private Object currentWeekType;
    @SerializedName("bin_colors")
    @Expose
    private String binColors;
    @SerializedName("collection_frequencies")
    @Expose
    private String collectionFrequencies;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("item_garbage")
    @Expose
    private String itemGarbage;
    @SerializedName("item_organic")
    @Expose
    private String itemOrganics;
    @SerializedName("item_recycling")
    @Expose
    private String itemRecycling;

    /**
     *
     * @return
     * The unitNumber
     */
    public String getUnitNumber() {
        return unitNumber;
    }

    /**
     *
     * @param unitNumber
     * The unit_number
     */
    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    /**
     *
     * @return
     * The houseNumber
     */
    public String getHouseNumber() {
        return houseNumber;
    }

    /**
     *
     * @param houseNumber
     * The house_number
     */
    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    /**
     *
     * @return
     * The street
     */
    public String getStreet() {
        return street;
    }

    /**
     *
     * @param street
     * The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     *
     * @return
     * The suburb
     */
    public String getSuburb() {
        return suburb;
    }

    /**
     *
     * @param suburb
     * The suburb
     */
    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    /**
     *
     * @return
     * The postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     *
     * @param postcode
     * The postcode
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     *
     * @return
     * The collectionDay
     */
    public String getCollectionDay() {
        return collectionDay;
    }

    /**
     *
     * @param collectionDay
     * The collection_day
     */
    public void setCollectionDay(String collectionDay) {
        this.collectionDay = collectionDay;
    }

    /**
     *
     * @return
     * The atlWeek
     */
    public String getAtlWeek() {
        return atlWeek;
    }

    /**
     *
     * @param atlWeek
     * The atl_week
     */
    public void setAtlWeek(String atlWeek) {
        this.atlWeek = atlWeek;
    }

    /**
     *
     * @return
     * The propertyP
     */
    public String getPropertyP() {
        return propertyP;
    }

    /**
     *
     * @param propertyP
     * The property_p
     */
    public void setPropertyP(String propertyP) {
        this.propertyP = propertyP;
    }

    /**
     *
     * @return
     * The pfi
     */
    public String getPfi() {
        return pfi;
    }

    /**
     *
     * @param pfi
     * The pfi
     */
    public void setPfi(String pfi) {
        this.pfi = pfi;
    }

    /**
     *
     * @return
     * The binType
     */
    public String getBinType() {
        return binType;
    }

    /**
     *
     * @param binType
     * The bin_type
     */
    public void setBinType(String binType) {
        this.binType = binType;
    }

    /**
     *
     * @return
     * The dateTime
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     *
     * @param dateTime
     * The date_time
     */
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    /**
     *
     * @return
     * The currentWeekType
     */
    public Object getCurrentWeekType() {
        return currentWeekType;
    }

    /**
     *
     * @param currentWeekType
     * The current_week_type
     */
    public void setCurrentWeekType(Object currentWeekType) {
        this.currentWeekType = currentWeekType;
    }

    /**
     *
     * @return
     * The binColors
     */
    public String getBinColors() {
        return binColors;
    }

    /**
     *
     * @param binColors
     * The bin_colors
     */
    public void setBinColors(String binColors) {
        this.binColors = binColors;
    }

    /**
     *
     * @return
     * The collectionFrequencies
     */
    public String getCollectionFrequencies() {
        return collectionFrequencies;
    }

    /**
     *
     * @param collectionFrequencies
     * The collection_frequencies
     */
    public void setCollectionFrequencies(String collectionFrequencies) {
        this.collectionFrequencies = collectionFrequencies;
    }

    /**
     *
     * @return
     * The area
     */
    public String getArea() {
        return area;
    }

    /**
     *
     * @param area
     * The area
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     *
     * @return
     * The itemGarbage
     */
    public String getItemGarbage() {
        return itemGarbage;
    }

    /**
     *
     * @param itemGarbage
     * The item_garbage
     */
    public void setItemGarbage(String itemGarbage) {
        this.itemGarbage = itemGarbage;
    }

    public String getStreet_type() {
        return street_type;
    }

    public void setStreet_type(String street_type) {
        this.street_type = street_type;
    }

    /**
     *
     * @return
     * The itemOrganics
     */
    public String getItemOrganics() {
        return itemOrganics;
    }

    /**
     *
     * @param itemOrganics
     * The item_organics
     */

    public void setItemOrganics(String itemOrganics) {
        this.itemOrganics = itemOrganics;
    }

    /**
     *
     * @return
     * The itemRecycling
     */
    public String getItemRecycling() {
        return itemRecycling;
    }

    /**
     *
     * @param itemRecycling
     * The item_recycling
     */
    public void setItemRecycling(String itemRecycling) {
        this.itemRecycling = itemRecycling;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }
}
