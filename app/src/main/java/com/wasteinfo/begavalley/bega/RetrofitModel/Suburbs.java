package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Test on 6/24/2016.
 */
public class Suburbs {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("suburbs")
    @Expose
    private List<String> suburbs = new ArrayList<String>();
    @SerializedName("streets")
    @Expose
    private List<String> streets = new ArrayList<String>();

    /**
     *
     * @return
     * The code
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The suburbs
     */
    public List<String> getSuburbs() {
        return suburbs;
    }

    /**
     *
     * @param suburbs
     * The suburbs
     */
    public void setSuburbs(List<String> suburbs) {
        this.suburbs = suburbs;
    }

    public List<String> getStreets() {
        return streets;
    }

    public void setStreets(List<String> streets) {
        this.streets = streets;
    }
}
