package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ProblemTypes {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("problem_types")
    @Expose
    private List<ProblemType> problemTypes = new ArrayList<ProblemType>();

    /**
     *
     * @return
     * The code
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The problemTypes
     */
    public List<ProblemType> getProblemTypes() {
        return problemTypes;
    }

    /**
     *
     * @param problemTypes
     * The problem_types
     */
    public void setProblemTypes(List<ProblemType> problemTypes) {
        this.problemTypes = problemTypes;
    }

}
