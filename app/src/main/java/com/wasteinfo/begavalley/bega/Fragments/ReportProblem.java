package com.wasteinfo.begavalley.bega.Fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Selection;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.HelperClasses.GPSTracker;
import com.wasteinfo.begavalley.bega.HelperClasses.KeyboardHelper;
import com.wasteinfo.begavalley.bega.HelperClasses.ReportSpinnerAdapter;
import com.wasteinfo.begavalley.bega.RetrofitAPI.MoiraAPI;
import com.wasteinfo.begavalley.bega.RetrofitModel.PostModel;
import com.wasteinfo.begavalley.bega.RetrofitModel.ProblemType;
import com.wasteinfo.begavalley.bega.RetrofitModel.Results;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.db.ProblemTypeDataSource;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ReportProblem extends Fragment implements View.OnClickListener,AdapterView.OnItemSelectedListener{

    private static final String LOGTAG = "WasteInfoVal";
    AddressActivity addressActivity;
    private Context context;
    private Library library;
    private List<String> list;
    private List<ProblemType> problemTypes;
    private int item_select;
    private String image_url;
    private File image_file;
    private SharedPref shared;

    private TextView label_subtitle;
    private TextView label_instruction;
    private EditText name;
    private EditText e_mail;
    private EditText phone;
    private EditText location_txt;
    private EditText notes;
    private CheckBox disclaimer;
    private ImageButton btn_camera;
    private ImageView image;
    private Spinner type_problem;
    private Button btn_send;
    private ImageButton btn_location;
    private OkHttpClient okHttpClient;
    private String url;

    private ProblemTypeDataSource problemTypeDataSource;
    private KeyboardHelper kb_helper;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.addressActivity = (AddressActivity)context;
        this.url = context.getResources().getString(R.string.url);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_report_problem, container, false);

        this.context = container.getContext();
        this.library = new Library(this.context);
        this.image_url="";

        kb_helper = new KeyboardHelper();

        shared = new SharedPref(addressActivity);


        this.label_subtitle = (TextView) rootview.findViewById(R.id.report_label_subtitle);
        this.label_instruction = (TextView) rootview.findViewById(R.id.report_label_instruction);
        this.name = (EditText) rootview.findViewById(R.id.report_name);
        this.e_mail = (EditText) rootview.findViewById(R.id.report_email);
        this.phone = (EditText) rootview.findViewById(R.id.report_phone);
        this.disclaimer = (CheckBox) rootview.findViewById(R.id.report_disclaimer);
        this.btn_camera = (ImageButton) rootview.findViewById(R.id.report_btn_camera);
        this.image = (ImageView) rootview.findViewById(R.id.report_image);
        this.type_problem = (Spinner) rootview.findViewById(R.id.report_type_problems);
        this.location_txt = (EditText) rootview.findViewById(R.id.report_location);
        this.notes = (EditText) rootview.findViewById(R.id.report_notes);
        this.btn_send = (Button) rootview.findViewById(R.id.report_btn_send);
        this.btn_location = (ImageButton) rootview.findViewById(R.id.location_btn);

        addressActivity.tv_title.setText(this.context.getResources().getString(R.string.report_problem));
        addressActivity.toggle.setDrawerIndicatorEnabled(true);
 ;
        kb_helper.setupUI(rootview.findViewById(R.id.report_lLactivity), addressActivity);
        kb_helper.setupUI(rootview.findViewById(R.id.LL_main), addressActivity);


        this.problemTypeDataSource = new ProblemTypeDataSource(this.context);
        this.problemTypeDataSource.open();

        this.problemTypes = this.problemTypeDataSource.getAllProblemTypes();

        this.problemTypeDataSource.close();

        this.list = new ArrayList<String>();
        try {
            if (!problemTypes.isEmpty()) {
                this.list.add("Choose a Topic");
                // Treatment spinner
                int j = 0;
                for (int i = 0; i < this.problemTypes.size(); i++) {

                        this.list.add(this.problemTypes.get(i)
                                .getTitle());
                        j++;
                        shared.setKeyValues("Pid" + j, this.problemTypes.get(i).getId());
                            int test  = shared.getIntValues("Pid"+j);


                }

                this.type_problem.setAdapter(new ReportSpinnerAdapter(this.context,
                        this.list));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.btn_send.setTypeface(this.library.getDefaultFont("regular"));
        name.setTypeface(this.library.getDefaultFont("regular"));
        location_txt.setTypeface(this.library.getDefaultFont("regular"));
        notes.setTypeface(this.library.getDefaultFont("regular"));
        e_mail.setTypeface(this.library.getDefaultFont("regular"));
        phone.setTypeface(this.library.getDefaultFont("regular"));
        label_subtitle.setTypeface(this.library.getDefaultFont("regular"));
        label_instruction.setTypeface(this.library.getDefaultFont("regular"));
        disclaimer.setTypeface(this.library.getDefaultFont("regular"));

        this.type_problem.setOnItemSelectedListener(this);
        this.btn_camera.setOnClickListener(this);
        this.btn_send.setOnClickListener(this);
        this.btn_location.setOnClickListener(this);
        this.image.setOnClickListener(this);

        return rootview;
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == this.btn_location.getId()) {
            permissionCheckLocation();

        }

        if (this.btn_camera.getId() == v.getId()) {
            permissionCheckCamera();
        }

        if (this.btn_send.getId() == v.getId()) {
            InputMethodManager imm = (InputMethodManager) addressActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(addressActivity.getCurrentFocus().getWindowToken(), 0);

            if(this.library.isConnectingToInternet()) {
                int test = this.item_select;
                if (this.item_select != 0
                        && !this.location_txt.getText().toString().isEmpty()
                        && !this.notes.getText().toString().isEmpty()
                        && !this.name.getText().toString().isEmpty()
                        && !this.e_mail.getText().toString().isEmpty()

                        ) {

                    if (this.e_mail!=null&&!this.e_mail.getText().toString().isEmpty() && !isValid(e_mail.getText().toString())) {
                        Toast.makeText(context, "Please enter a valid email address.", Toast.LENGTH_LONG).show();
                    } else {



                        String type = Integer.toString(shared.getIntValues("Pid" + this.item_select));
                        String location = this.location_txt.getText().toString();
                        String notes = this.notes.getText().toString();
                        String name = this.name.getText().toString();
                        String email = this.e_mail.getText().toString();
                        String phone = this.phone.getText().toString();
                        String image_url = this.image_url;
                        boolean contact = this.disclaimer.isChecked();
                        HashMap<String, String> formValues = new HashMap<String, String>();
                        HashMap<String, String> fileValues = new HashMap<String, String>();

                        formValues.put("name", name);
                        formValues.put("email", email);
                        formValues.put("problem_type_id", type);
                        formValues.put("phone_no", phone);
                        formValues.put("location", location);
                        formValues.put("notes", notes);

                        fileValues.put("image", image_url);
                        boolean uploadFile = false;
                        if (image_url.isEmpty()) {
                            uploadFile = false;
                        } else {
                            uploadFile = true;
                        }
                        postdata(formValues,fileValues);


                        //SendReportLive report=new SendReportLive(this.context, type, location, notes, name, email, phone, image_url, contact);
                        //report.execute();
                        this.btn_camera.setVisibility(View.VISIBLE);
                        this.location_txt.getText().clear();
                        this.notes.getText().clear();
                        this.name.getText().clear();
                        this.e_mail.getText().clear();
                        this.phone.getText().clear();
                        this.disclaimer.setChecked(false);
                        this.type_problem.setSelection(0);
                        this.image.setImageResource(android.R.color.transparent);
                    }


                } else if (this.item_select == 0) {
//                Toast toast = Toast.makeText(this.context, "Please choose a topic, location and write note", Toast.LENGTH_LONG);
                    Toast toast = Toast.makeText(this.context, "Please choose a topic", Toast.LENGTH_LONG);
                    toast.show();

                } else if (this.location_txt.getText().toString().isEmpty()) {
                    Toast toast = Toast.makeText(this.context, "Please insert location", Toast.LENGTH_LONG);
                    toast.show();

                } else if (this.notes.getText().toString().isEmpty()) {
                    Toast toast = Toast.makeText(this.context, "Please write a note", Toast.LENGTH_LONG);
                    toast.show();

                }
            }
            else{
                Toast.makeText(this.context,"No Internet Connection",Toast.LENGTH_LONG).show();
            }
//            else if (TextUtils.isEmpty(this.name.getText().toString())){
//                Toast toast = Toast.makeText(this.context, "Please enter your name", Toast.LENGTH_LONG);
//                toast.show();
//            }
//            else if (TextUtils.isEmpty(this.e_mail.getText().toString())){
//                Toast toast = Toast.makeText(this.context, "Please enter your email", Toast.LENGTH_LONG);
//                toast.show();
//            }
//            else if(!isValid(this.e_mail.getText().toString())){
//
//                Toast toast = Toast.makeText(this.context, "Please enter a valid email", Toast.LENGTH_LONG);
//                toast.show();
//
//            }
//            else if (TextUtils.isEmpty(this.phone.getText().toString())){
//                Toast toast = Toast.makeText(this.context, "Please enter your phone number", Toast.LENGTH_LONG);
//                toast.show();
//            }
//            else if (!this.disclaimer.isChecked()){
//                Toast toast = Toast.makeText(this.context, "Please check contact me for further details", Toast.LENGTH_LONG);
//                toast.show();
//            }

        }
        if (v.getId() == this.image.getId()) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takePictureIntent, 666);
        }

    }

    private void  setMyLocation() {
        if(this.library.isConnectingToInternet()) {
            InputMethodManager imm = (InputMethodManager) addressActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(addressActivity.getCurrentFocus().getWindowToken(), 0);
            Log.d(LOGTAG, "location button clicked!");
//            final ProgressDialog pd = ProgressDialog.show(this.context, "", "Loading location...", true, true);
//
//            MyLocation.LocationsResult locationResult = new MyLocation.LocationsResult() {
//                @Override
//                public void gotLocation(Location location) {
//                    //Got the location!
//                    if (location != null) {
//
//                        //get latitude and longitude of the location
//                        double lng = location.getLongitude();
//                        double lat = location.getLatitude();
//
//                        Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
//
//                        try {
//                            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
//
//                            if (addresses != null) {
//                                Address returnedAddress = addresses.get(0);
//                                StringBuilder strReturnedAddress = new StringBuilder();
//                                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
//                                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(" ");
//                                }
//                                String location_text = strReturnedAddress.toString();
//                                Log.d("Location success:", location_text);
//
//                                Handler handler = new Handler();
//                                setTextLocation(location_text);
//                                handler.postDelayed(new Runnable() {
//                                    public void run() {
//                                        pd.dismiss();
//                                    }
//                                }, 3000);
//
//
//                            } else {
//                                Toast.makeText(context, "No Location found", Toast.LENGTH_LONG).show();
//                            }
//                        } catch (IOException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                            Toast.makeText(context, "Timed out waiting for response from server. Please try again.", Toast.LENGTH_LONG).show();
////                                location_txt.setText("Cannot get Address!");
//                        }
//
//                    } else {
//                        Toast.makeText(context, "No Location found", Toast.LENGTH_LONG).show();
//                    }
//
//                }
//            };
//            MyLocation myLocation = new MyLocation();
//            myLocation.getLocation(this.context, locationResult);
//            if (!myLocation.gps_enabled && !myLocation.network_enabled) {
//                pd.dismiss();
//                // Build the alert dialog
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setTitle("Location Services Not Active");
//                builder.setMessage("Please enable Location Services and GPS");
//                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        // Show location settings when the user acknowledges the alert dialog
//                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                        startActivity(intent);
//                    }
//                });
//                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                });
//                Dialog alertDialog = builder.create();
//                alertDialog.setCanceledOnTouchOutside(true);
//                alertDialog.show();
////				showMessage();
//            }
            GPSTracker gps;
            final ProgressDialog pd = ProgressDialog.show(this.context, "", "Loading location...", true, true);

            gps = new GPSTracker(addressActivity);
            double lat,lng;
            String latlng = null;

            // Check if GPS enabled
            if(gps.canGetLocation()) {

                lat = gps.getLatitude();
                 lng= gps.getLongitude();
                latlng = String.valueOf(lat)+","+String.valueOf(lng);


                // \n is for new line
//            Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + this.MyLat + "\nLong: " + this.MyLang, Toast.LENGTH_LONG).show();
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }

            getLocation(pd, latlng);

        }
        else{
            Toast.makeText(context,"No internet connection",Toast.LENGTH_LONG).show();
        }
    }

    private void getLocation(final ProgressDialog pd, String latlng) {
        final String API = "https://maps.googleapis.com/maps/api/";
        retrofitClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MoiraAPI moiraAPI = retrofit.create(MoiraAPI.class);

        Call<Results> listCall = moiraAPI.getCurrentLocation(latlng);
        listCall.enqueue(new Callback<Results>() {
            @Override
            public void onResponse(Call<Results> call, Response<Results> response) {

                    location_txt.setText(response.body().results.get(0).formattedAddress);
                    int position = response.body().results.get(0).formattedAddress.length();
                    Editable et = location_txt.getText();

                pd.dismiss();
            }

            @Override
            public void onFailure(Call<Results> call, Throwable t) {
                Toast.makeText(addressActivity, "Failed", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        });
    }

    private void postdata(HashMap<String, String> formValues, HashMap<String, String> fileValues) {
        final ProgressDialog pd = ProgressDialog.show(this.context, "", "Sending report....", true, true);
        final String API = this.url;
        retrofitClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MoiraAPI moiraAPI = retrofit.create(MoiraAPI.class);

        Call<PostModel> upload = moiraAPI.sendFeedback(fileValues,formValues);
        upload.enqueue(new Callback<PostModel>() {
            @Override
            public void onResponse(Call<PostModel> call, Response<PostModel> response) {

                if (response.isSuccessful()) {
                    String code = response.body().success;
                    Toast.makeText(context, "Report sent successfully", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();

            }


            @Override
            public void onFailure(Call<PostModel> call, Throwable t) {
                Toast.makeText(context, "Report sending failed!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void permissionCheckLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && addressActivity.checkSelfPermission(
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && addressActivity.checkSelfPermission(
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    1200);
            return;
        } else {
            setMyLocation();
        }
    }
    private void setTextLocation(final String location_text) {

        addressActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                location_txt.setText(location_text);
                int position = location_text.length();
                Editable et = location_txt.getText();
                Selection.setSelection(et, position);
            }
        });
    }

    private void retrofitClient(){
        okHttpClient = new OkHttpClient.Builder()
                .readTimeout(20, TimeUnit.SECONDS)
                .connectTimeout(15, TimeUnit.SECONDS)
                .build();
    }

    private void permissionCheckCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && addressActivity.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.CAMERA},
                    1100);

            return;
        } else {
            permissionCheckStorage();
        }
    }

    private void openCameraIntent() {
        InputMethodManager imm = (InputMethodManager) addressActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(addressActivity.getCurrentFocus().getWindowToken(), 0);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePictureIntent, 666);
    }

    private void permissionCheckStorage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && addressActivity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1300);

            return;
        } else {
            openCameraIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    permissionCheckStorage();
                break;
            case 1200:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    setMyLocation();
                break;
            case 1300:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    openCameraIntent();
                break;

        }
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
        this.item_select = i;
        Log.d(LOGTAG, "Selected Item==" + this.item_select);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 666 && resultCode == addressActivity.RESULT_OK) {
            String photoPath = "";

            Bundle extras = data.getExtras();
            Bitmap mImageBitmap = (Bitmap) extras.get("data");
            this.image.setImageBitmap(mImageBitmap);
            this.btn_camera.setVisibility(View.INVISIBLE);

            Cursor cursor = addressActivity.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_ADDED, MediaStore.Images.ImageColumns.ORIENTATION}, MediaStore.Images.Media.DATE_ADDED, null, "date_added ASC");
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    Uri uri = Uri.parse(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA)));
                    photoPath = uri.toString();
                } while (cursor.moveToNext());
                cursor.close();
            }

            Log.d(LOGTAG, "Image Path=" + photoPath);
            this.image_url = photoPath;
            //Uri selectedImageUri = data.getData();
            //String selectedImagePath = getRealPathFromURI(this.context,selectedImageUri);

            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
            //Uri tempUri = getImageUri(getApplicationContext(), mImageBitmap);

            // CALL THIS METHOD TO GET THE ACTUAL PATH
            //File finalFile = new File(getRealPathFromURI(tempUri));

            //Log.d(LOGTAG,"final file==="+finalFile);

            //this.image_file=finalFile;
            /*SimpleDateFormat test = new SimpleDateFormat("yyyyMMddHHmmss");
            String file_name = test.format(new Date(System.currentTimeMillis()))+".png";
            try {
                FileOutputStream output;
                output = this.context.openFileOutput(file_name, Context.MODE_PRIVATE);
                mImageBitmap.compress(Bitmap.CompressFormat.PNG, 60, output);
                output.close();
                this.image_url = file_name;
            } catch (Exception e) {
                e.printStackTrace();
            }


            Log.d("PICTURE", file_name);
            this.image_url = file_name;
        */
        }
    }

    public boolean isValid(CharSequence target) {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()) {
            return true;
        } else {
            return false;
        }
    }

    }
