package com.wasteinfo.begavalley.bega.WebServiceTasks;

import android.content.Context;
import android.util.Log;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitAPI.MoiraAPI;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.RetrofitModel.ProblemType;
import com.wasteinfo.begavalley.bega.RetrofitModel.Sections;
import com.wasteinfo.begavalley.bega.RetrofitModel.UpdateModel;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.db.PageDataSource;
import com.wasteinfo.begavalley.bega.db.ProblemTypeDataSource;
import com.wasteinfo.begavalley.bega.db.SectionDataSource;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Test on 6/24/2016.
 */
public class UpdateDB {


    private Context context;
    private String url;
    SharedPref pref;
    SectionDataSource sectionDataSource;
    PageDataSource pageDataSource;
    ProblemTypeDataSource problemTypeDataSource;

    public UpdateDB(Context context) {

        this.context = context;
        this.url = context.getResources().getString(R.string.url);
        this.pref = new SharedPref(context);


    }

    //Retrofit response for update db
    public void getUpdated() {


        String ts = this.pref.getStringValues("TimeStamp");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(this.url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();
        MoiraAPI moiraAPI = retrofit.create(MoiraAPI.class);

        Call<UpdateModel> update = moiraAPI.getUpdatedData(ts);
        update.enqueue(new Callback<UpdateModel>() {
            @Override
            public void onResponse(Call<UpdateModel> call, Response<UpdateModel> response) {
                Sections sections = new Sections();
                Page page = new Page();
                ProblemType problemType = new ProblemType();
                sectionDataSource = new SectionDataSource(context);
                pageDataSource = new PageDataSource(context);
                problemTypeDataSource = new ProblemTypeDataSource(context);
                getSectionsUpdated(response, sections);
                getPageUpdated(response, page);
                getProblemTypesUpdated(response, problemType);
                pref.setKeyValues("TimeStamp", response.body().getDate_time());
                pref.setKeyValues("dbFlag",1);

            }

            @Override
            public void onFailure(Call<UpdateModel> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }
    //Inserting updated problem types
    private void getProblemTypesUpdated(Response<UpdateModel> response, ProblemType problemType) {
        problemTypeDataSource.open();
        for (int i = 0; i < response.body().getProblemTypes().size(); i++) {

            problemType.setId(response.body().getProblemTypes().get(i).getId());
            problemType.setTitle(response.body().getProblemTypes().get(i).getTitle());
            problemType.setEmail(response.body().getProblemTypes().get(i).getEmail());

            problemTypeDataSource.insertOrUpdateProblemType(problemType);

        }
        problemTypeDataSource.close();

    }

    //inserting or updating sections
    private void getSectionsUpdated(Response<UpdateModel> response, Sections sections) {
        sectionDataSource.open();
        List<Sections> listSections = new ArrayList<Sections>();
        if (response.isSuccessful()&&response.body().getMsg().toString().equalsIgnoreCase("Success")) {
            for (int i = 0; i < response.body().getSections().size(); i++) {

                long id = response.body().getSections().get(i).getId();
                sections.setId(id);
                sections.setParentId(response.body().getSections().get(i).getParentId());
                sections.setTitle(response.body().getSections().get(i).getTitle());
                sections.setStatus(response.body().getSections().get(i).getStatus());
                sections.setSectionId(response.body().getSections().get(i).getSectionId());

                sectionDataSource.insertOrUpdateSection(sections);
                System.out.println(sections.getId() + "Title: " + sections.getTitle());
            }
            sectionDataSource.close();
        }
        else{
            Log.d("Server error: ","Error in server");
        }
    }


    //inserting or updating pages
    private void getPageUpdated(Response<UpdateModel> response, Page page) {

        pageDataSource.open();

        for (int i = 0; i < response.body().getPage().size(); i++) {

            page.setId(response.body().getPage().get(i).getId());
            page.setPageTitle(response.body().getPage().get(i).getPageTitle());
            page.setPageDescription(response.body().getPage().get(i).getPageDescription());
            page.setPageContent(response.body().getPage().get(i).getPageContent());
            page.setPageImage(response.body().getPage().get(i).getPageImage());
            page.setSectionId(response.body().getPage().get(i).getSectionId());
            page.setTypeOfCalendar(response.body().getPage().get(i).getTypeOfCalendar());
            page.setLatitude(response.body().getPage().get(i).getLatitude());
            page.setLongitude(response.body().getPage().get(i).getLongitude());
            page.setOrder(response.body().getPage().get(i).getOrder());
            page.setPageStatus(response.body().getPage().get(i).getPageStatus());
            page.setAllowed(response.body().getPage().get(i).getAllowed());
            page.setNotAllowed(response.body().getPage().get(i).getNotAllowed());
            page.setBinTypeId(response.body().getPage().get(i).getBinTypeId());
            page.setBinColorId(response.body().getPage().get(i).getBinColorId());

            System.out.println(page.getId() + "Title: " + page.getPageTitle());
            pageDataSource.insertOrUpdatePage(page);
        }
        pageDataSource.close();


    }
}
