package com.wasteinfo.begavalley.bega.RetrofitAPI;


import com.wasteinfo.begavalley.bega.RetrofitModel.Location;
import com.wasteinfo.begavalley.bega.RetrofitModel.PostModel;
import com.wasteinfo.begavalley.bega.RetrofitModel.ProblemTypes;
import com.wasteinfo.begavalley.bega.RetrofitModel.Results;
import com.wasteinfo.begavalley.bega.RetrofitModel.Section;
import com.wasteinfo.begavalley.bega.RetrofitModel.Suburbs;
import com.wasteinfo.begavalley.bega.RetrofitModel.UpdateModel;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Test on 6/24/2016.
 */
public interface MoiraAPI {

    @FormUrlEncoded
    @POST("get_problem_types")
    Call<ProblemTypes> getProblemTypes(@Field("test") String test);

    @FormUrlEncoded
    @POST("location/suburb_search")
    Call<Suburbs> getSuburbs(@Field("suburb") String suburb);

    @FormUrlEncoded
    @POST("location/search")
    Call<Location> getLocations(@Field("suburb") String suburb,
                                @Field("address") String address);
    @FormUrlEncoded
    @POST("location/auto_suggest")
    Call<Suburbs> getAutoStreet(@Field("suburb") String suburb);

    @FormUrlEncoded
    @POST("get_updated_data")
    Call<UpdateModel> getUpdatedData(@Field("datetime") String datetime);

    @FormUrlEncoded
    @POST("update_device_location")
    Call<String> updateDevice(@Field("device_uuid") String deviceid,
                              @Field("street") String street,
                              @Field("suburb") String suburb,
                              @Field("device_type") String device_type);

    @FormUrlEncoded
    @POST("register_device")
    Call<String> registerDevice(@Field("device_uuid") String deviceid,
                              @Field("device_token") String device_token,
                              @Field("device_type") String device_type);

    @FormUrlEncoded
    @POST("section/get_section_pages_by_section_id")
    Call<Section> getSection(@Field("section_id") int section,
                             @Field("bin_type") int binType);

    @FormUrlEncoded
    @POST("feedback")
    Call<PostModel> sendFeedback(@FieldMap HashMap<String,String> file,
                                 @FieldMap HashMap<String,String> params);

    @GET("geocode/json")
    Call<Results> getCurrentLocation(@Query("latlng") String latlng);



}
