package com.wasteinfo.begavalley.bega.WebServiceTasks;

import android.content.Context;
import android.util.Log;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitAPI.MoiraAPI;
import com.wasteinfo.begavalley.bega.RetrofitModel.ProblemTypes;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Test on 6/24/2016.
 */
public class GetProblemType {
    final String API = "http://ci.draftserver.com/moirashireapp/webservice/";
    Call<ProblemTypes> problemTypeCall;
    private Context context;
    String url;

    public GetProblemType(Context context){
        this.context = context;
        this.url = context.getResources().getString(R.string.url);

    }

   public void getProblemTypes(){
       Retrofit retrofit = new Retrofit.Builder()
               .baseUrl(this.url)
               .client(new OkHttpClient())
               .addConverterFactory(GsonConverterFactory.create())
               .build();
       MoiraAPI moiraAPI = retrofit.create(MoiraAPI.class);

       problemTypeCall = moiraAPI.getProblemTypes("1");
       problemTypeCall.enqueue(new Callback<ProblemTypes>() {
           @Override
           public void onResponse(Call<ProblemTypes> call, Response<ProblemTypes> response) {

               Log.d("Problem Type","msg: "+"Success");
           }

           @Override
           public void onFailure(Call<ProblemTypes> call, Throwable t) {
               t.printStackTrace();
           }
       });
   }




}
