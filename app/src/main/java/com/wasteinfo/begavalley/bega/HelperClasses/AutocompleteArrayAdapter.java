package com.wasteinfo.begavalley.bega.HelperClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.R;

import java.util.ArrayList;

public class AutocompleteArrayAdapter extends ArrayAdapter<String> {

	private Context context;
    private Library library;
    private ArrayList<String> data;
	
	public AutocompleteArrayAdapter(Context context, ArrayList<String> data) {
		super(context, R.layout.list_item_suggestions,data);
		this.context=context;
		this.library=new Library(this.context);
		this.data = data;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView;

        rowView = inflater.inflate(R.layout.list_item_suggestions, parent, false);
        TextView suggestion = (TextView) rowView.findViewById(R.id.suggestion_text);
        suggestion.setText(getItem(position).toString());
//		suggestion.setMaxHeight(position);
        suggestion.setTypeface(this.library.getDefaultFont("regular"));



        return rowView;
		
	}

}
