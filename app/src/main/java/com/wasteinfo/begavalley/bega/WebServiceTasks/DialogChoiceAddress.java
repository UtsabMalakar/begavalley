package com.wasteinfo.begavalley.bega.WebServiceTasks;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.Fragments.CalendarFragment;
import com.wasteinfo.begavalley.bega.HelperClasses.DialogChoiceAddressArrayAdapter;
import com.wasteinfo.begavalley.bega.HelperClasses.StringTreatment;
import com.wasteinfo.begavalley.bega.RetrofitModel.LocationResult;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Test on 6/30/2016.
 */
public class DialogChoiceAddress extends Dialog implements View.OnClickListener, ListView.OnItemClickListener {


    private AddressActivity activity;
    private Library library;
    private TextView title;
    private ListView list;
    private Button btn_cancel;
    private ArrayList<Calendar> date_list;
    private Context context;
    private SharedPreferences datePrefs, getuuid;
    private SharedPref pref;
    private Activity current;
    private Intent intent_address;
    private Intent intent_calendar;
    private List<LocationResult> locations;
    private String collection_check;
    com.wasteinfo.begavalley.bega.db.LocationDataSource datasource;

    private String device_uid;
    private static final String LOGTAG = "WasteInfoVal";
    private UpdateDeviceLocation updateDeviceLocation;


    public DialogChoiceAddress(Context context) {
        super(context);
    }

    public DialogChoiceAddress(Context context, List<LocationResult> location) {
        super(context);

        activity = (AddressActivity) context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_choice_address);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.locations = location;

        this.context = context;
        this.library = new Library(this.context);
        this.datePrefs = context.getSharedPreferences("DATE_PREFS_FILE", Context.MODE_PRIVATE);
        this.getuuid = PreferenceManager.getDefaultSharedPreferences(context);
        this.current = (Activity) context;
        this.title = (TextView) findViewById(R.id.dialog_choice_address_title);
        this.list = (ListView) findViewById(R.id.dialog_choice_address_list);
        this.btn_cancel = (Button) findViewById(R.id.dialog_choice_address_btn);
        pref = new SharedPref(this.context);
        updateDeviceLocation = new UpdateDeviceLocation(this.context);
        //this.collection_check=collection_check;
        Log.d("testVal", "Inside dialog class");

        for (LocationResult obj : locations) {
            Log.i("Data", obj.toString());
        }

        //ArrayAdapter<AddressList> adapter = new DialogChoiceAddressArrayAdapter(context, list);

        //Treatment fonts
        this.title.setTypeface(this.library.getDefaultFont("bold"));
        this.btn_cancel.setTypeface(this.library.getDefaultFont("regular"));


        //Listener
        this.list.setAdapter(new DialogChoiceAddressArrayAdapter(this.context, this.locations));
        this.list.setOnItemClickListener(this);
        this.btn_cancel.setOnClickListener(this);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onClick(View view) {
        if (this.btn_cancel.getId() == view.getId()) {
            this.dismiss();
            //this.current.finish();
            /*this.context.startActivity(this.intent_address);
            SharedPreferences pref = this.context.getSharedPreferences("MyPref", 0); // 0 - for private mode
            String val=pref.getString("user_address", null);
            Log.d("testVal",val);*/
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        LocationResult address = this.locations.get(i);
        if (this.library.isConnectingToInternet()) {


            //LocationResult address = this.locations.get(i);

            //AddressList address = this.addresses.get(i);
            datasource = new com.wasteinfo.begavalley.bega.db.LocationDataSource(this.context);
            datasource.open();

            address = datasource.insertLocation(address);
            datasource.close();
            Log.d(LOGTAG, "LocationResult inserted with id==" + address.getItemId());


            pref.setKeyValues("item_id", (int) address.getItemId());
            int length = address.getStreet().length();
            StringTreatment treatments = new StringTreatment();
            if (length > 20) {
//						String housenumber = String.valueOf(address.getHouseNumber());
                String treated_string = treatments.treatment(address.getStreet());
                address.setStreet(treated_string);

            }
            pref.setKeyValues("street_name", address.getStreet());
            //pref.putInt("bin_type_id", address.getBinType());
            pref.setKeyValues("house_number", address.getHouseNumber());
            pref.setKeyValues("collection_day", address.getCollectionDay());
            pref.setKeyValues("collection_week", address.getCollectionDay());
            pref.setKeyValues("collection_frequencies", address.getCollectionFrequencies());
            pref.setKeyValues("week_type_match", address.getAtlWeek());
            pref.setKeyValues("item_garbage", address.getItemGarbage());
            pref.setKeyValues("item_organic", address.getItemOrganics());
            pref.setKeyValues("item_recycling", address.getItemRecycling());
            pref.setKeyValues("bin_colors", address.getBinColors());
            pref.setKeyValues("suburb", address.getSuburb());
            pref.setKeyValues("Area", address.getArea());
            pref.setKeyValues("date_time", address.getDateTime());
            String test = address.getItemGarbage();

            int all = pref.getIntValues("all_check");
            int next = pref.getIntValues("next_check");

            CalendarFragment calendarFragment = new CalendarFragment();
            Bundle args = new Bundle();
            args.putInt("section_id", 3);
            calendarFragment.setArguments(args);
            activity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentID, calendarFragment).commit();

//          this.current.finish();
            DialogChoiceAddress.this.dismiss();
            device_uid = pref.getStringValues("device_uuid");
            updateDeviceLocation.UpdateDevice(device_uid, address.getStreet(), address.getSuburb(), "2");
//          Toast.makeText(this.context, "Suburb: "+address.getSuburb()+"Street: "+address.getStreet(), Toast.LENGTH_LONG).show();


			/*AsyncTaskUpdateWeekly asyncTaskUpdateWeekly = new AsyncTaskUpdateWeekly(this.context);
            String locationId=Long.toString(address.getItemId());
			//Log.d(LOGTAG,"Collection_Check="+this.collection_check);
            asyncTaskUpdateWeekly.execute(locationId,this.collection_check);*/

        } else {
            Toast toast = Toast.makeText(this.context, "No internet connection!", Toast.LENGTH_SHORT);
            toast.show();
        }

        //this.current.finish();
        DialogChoiceAddress.this.dismiss();


    }


}
