package com.wasteinfo.begavalley.bega.HelperClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitModel.LocationResult;

import java.util.List;

public class DialogChoiceAddressArrayAdapter extends ArrayAdapter {

	Context context;
	List<LocationResult> list;
	Library library;
	
	
	public DialogChoiceAddressArrayAdapter(Context context, List<LocationResult> list ) {
		super(context,android.R.id.content,list);
		this.context=context;
		this.list=list;
		this.library=new Library(this.context);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = vi.inflate(R.layout.list_item_choice_address, null);
		LocationResult LocationResult = list.get(position);
		
	
		TextView tv_address = (TextView) view.findViewById(R.id.list_item_choice_address_value);
		String address = LocationResult.getStreet()+" "+LocationResult.getHouseNumber();
		tv_address.setText(address);
		
	
		//treatment fonts
		tv_address.setTypeface(this.library.getDefaultFont("regular"));
		
		
		return view;
	}
	
}
