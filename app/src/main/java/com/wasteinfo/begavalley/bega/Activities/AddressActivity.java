package com.wasteinfo.begavalley.bega.Activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.AlarmServices.AlarmNotificationReceiver;
import com.wasteinfo.begavalley.bega.Fragments.AddressFragment;
import com.wasteinfo.begavalley.bega.Fragments.CalendarFragment;
import com.wasteinfo.begavalley.bega.Fragments.Material;
import com.wasteinfo.begavalley.bega.Fragments.ReportProblem;
import com.wasteinfo.begavalley.bega.Fragments.WasteMaterials;
import com.wasteinfo.begavalley.bega.HelperClasses.AutocompleteArrayAdapter;
import com.wasteinfo.begavalley.bega.HelperClasses.KeyboardHelper;
import com.wasteinfo.begavalley.bega.HelperClasses.ObjectSerializer;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.WebServiceTasks.GetLocation;
import com.wasteinfo.begavalley.bega.WebServiceTasks.GetSuburbs;
import com.wasteinfo.begavalley.bega.db.LocationDataSource;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.wasteinfo.begavalley.R.id.address_settings;
import static com.wasteinfo.begavalley.R.id.et_street_no;
import static com.wasteinfo.begavalley.R.id.suburb_settings;

public class AddressActivity extends AppCompatActivity implements AHBottomNavigation.OnTabSelectedListener,
        NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener,
        View.OnFocusChangeListener {

    private static final String LOGTAG = "WasteInfo";
    public AHBottomNavigation bottomNavigation;
    public TextView tv_title;
    private RadioButton radio_resident;
    private RadioButton radio_guest;
    private LinearLayout linearLayout;
    private Button btn_proceed;
    private AutoCompleteTextView base_suburb;
    private AutoCompleteTextView base_street;
    private EditText et_street_no;
    String push_notification_message;
    private Button btn_set_alarm;
    private Button btn_organic, btn_recycling, btn_garbage;
    private CheckBox next_collection;
    private CheckBox all_collection;
    private TextView all;
    private TextView next;
    String organic_date,garbage_date;

    private long item_id;
    private int bin_type_id;
    private String bin_colors;
    private ArrayList<Calendar> date_list;

    private GetSuburbs getAddressAuto;
    private GetLocation getLocation;
    public DrawerLayout drawer;
    AlarmNotificationReceiver alm;


    private ArrayList<String> suburb_list;
    private ArrayList<String> addressAuto;

    private AutocompleteArrayAdapter adapter;


    private Library library;
    public ActionBarDrawerToggle toggle;
    private SharedPref pref;
    private String checkRadio = "2";
    private LocationDataSource datasource;
    private KeyboardHelper kb_helper;
    private String recycling_date;
    private Calendar garbage_cal;
    private Calendar recycle_cal;
    private Calendar organic_cal;

//    private KeyboardHelper kb_helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        kb_helper = new KeyboardHelper();
//        kb_helper.setupUI(findViewById(R.id.sliding_layout), this);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.sliding_menu_button);



        this.library = new Library(this);
        pref = new SharedPref(this);
        alm = new AlarmNotificationReceiver();


        this.getAddressAuto = new GetSuburbs(this);
        this.getLocation = new GetLocation(this);

//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayShowHomeEnabled(false);
//        actionBar.setDisplayShowCustomEnabled(true);
//        actionBar.setDisplayShowTitleEnabled(false);
//        View view = getLayoutInflater().inflate(R.layout.action_bar,
//                null);
//        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
//                ActionBar.LayoutParams.MATCH_PARENT);
//        actionBar.setCustomView(view, layoutParams);
//        Toolbar parent = (Toolbar) view.getParent();

        drawer = (DrawerLayout) findViewById(R.id.drayer_id);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //change the menu icon
        toggle.setDrawerIndicatorEnabled(false);


        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                base_suburb.getText().clear();
                base_street.getText().clear();
                ButtonHide();
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {



            }
        });




        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerview = navigationView.getHeaderView(0);

//        parent.setContentInsetsAbsolute(0, 0);

        tv_title = (TextView) toolbar.findViewById(R.id.title_text);
        btn_proceed = (Button) headerview.findViewById(R.id.baseproceed_settings);
        base_suburb = (AutoCompleteTextView) headerview.findViewById(suburb_settings);
        base_street = (AutoCompleteTextView) headerview.findViewById(address_settings);
        et_street_no = (EditText) headerview.findViewById(R.id.bd_et_street_no);
        btn_set_alarm = (Button) headerview.findViewById(R.id.reminder_btn);
        btn_garbage = (Button) headerview.findViewById(R.id.menu_btn_garbage);
        btn_recycling = (Button) headerview.findViewById(R.id.menu_btn_recycling);
        btn_organic = (Button) headerview.findViewById(R.id.menu_btn_organics);


        all = (TextView) headerview.findViewById(R.id.reminder_label_all_collections);
        next = (TextView) headerview.findViewById(R.id.reminder_label_next_collection);

        next_collection = (CheckBox) headerview.findViewById(R.id.checkbox_next_collection);
        all_collection = (CheckBox) headerview.findViewById(R.id.reminder_checkbox_all_collections);

        try {
            suburb_list = (ArrayList<String>) ObjectSerializer.deserialize(pref.getStringValues("Suburbs"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.adapter = new AutocompleteArrayAdapter(this, suburb_list);
        this.base_suburb.setAdapter(adapter);
        this.base_suburb.setThreshold(1);
        kb_helper.setupUI(headerview.findViewById(R.id.Rl_base), this);
        kb_helper.setupUI(headerview.findViewById(R.id.Ll_base), this);

        checkHandler();


        btn_proceed.setOnClickListener(this);
        btn_set_alarm.setOnClickListener(this);
//        this.base_street.setOnFocusChangeListener(this);


        tv_title.setTypeface(this.library.getDefaultFont("regular"));
        this.btn_proceed.setTypeface(this.library.getDefaultFont("regular"));
        this.base_street.setTypeface(this.library.getDefaultFont("regular"));
        this.base_suburb.setTypeface(this.library.getDefaultFont("regular"));
        this.et_street_no.setTypeface(this.library.getDefaultFont("regular"));
//        this.radio_guest.setTypeface(this.library.getDefaultFont("bold"));
//        this.radio_resident.setTypeface(this.library.getDefaultFont("bold"));
        this.btn_set_alarm.setTypeface(this.library.getDefaultFont("regular"));
        this.all.setTypeface(this.library.getDefaultFont("regular"));
        this.next.setTypeface(this.library.getDefaultFont("regular"));

        linearLayout = (LinearLayout) findViewById(R.id.contentID);
//        kb_helper = new KeyboardHelper();
//        kb_helper.setupUI(findViewById(R.id.add_activity), this);
//        kb_helper.setupUI(findViewById(R.id.LlayoutId), this);

        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation_id);
        bottomNavigation.setOnTabSelectedListener(this);
        this.createNavBarItems();


    }

    public void ButtonHide() {
        this.item_id = pref.getIntValues("item_id");
        datasource = new LocationDataSource(this);
        datasource.open();
        String extractedOrganicDate = datasource.getOrganicDateByItemId(item_id);

        if (extractedOrganicDate == null || extractedOrganicDate.isEmpty()) {
            this.btn_organic.setVisibility(View.GONE);
//            this.reminder_frame.getLayoutParams().width = 1;
        }
        else{
            this.btn_organic.setVisibility(View.VISIBLE);
        }

        String extractedRecyclingDate = datasource.getRecyclingDateByItemId(item_id);
        if (extractedRecyclingDate == null || extractedRecyclingDate.isEmpty()) {
            this.btn_recycling.setVisibility(View.GONE);

        }
        else{
            this.btn_recycling.setVisibility(View.VISIBLE);
        }

        String extractedGarbageDate = datasource.getGarbageDateByItemId(item_id);
        if (extractedGarbageDate == null || extractedGarbageDate.isEmpty()) {
            this.btn_garbage.setVisibility(View.GONE);
        }
        else{
            this.btn_garbage.setVisibility(View.VISIBLE);
        }
    }


    private void checkHandler() {

        if (pref != null) {
            int check_flag_all = pref.getIntValues("check_flag_all");
            int check_flag_next = pref.getIntValues("check_flag_next");

            if (check_flag_all == 1) {
                this.all_collection.setChecked(true);
            } else {
                this.all_collection.setChecked(false);
            }
            if (check_flag_next == 1) {
                this.next_collection.setChecked(true);
            } else {
                this.next_collection.setChecked(false);
            }
        }

        if (!all_collection.isChecked() && next_collection.isChecked()) {
            boolean check = alm.getcheckNext();
            if (check == false) {
                next_collection.setChecked(false);
                Log.d("Check next", "unchecked");
            }

        }
        checkRadio = "2";
//        radio_resident.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkRadio = "2";
//                base_suburb.setEnabled(true);
//                base_street.setEnabled(true);
//                radio_guest.setChecked(false);
//            }
//        });
//
//        radio_guest.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkRadio = "1";
//                base_suburb.setEnabled(false);
//                base_suburb.setText("");
//                base_street.setEnabled(false);
//                radio_resident.setChecked(false);
//
//            }
//        });

        this.all_collection
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (all_collection.isChecked()) {
                            Log.d(LOGTAG, "All collection checked!");
                            next_collection.setChecked(true);
                        } else {
                            Log.d(LOGTAG, "All collection unchecked!");
                            next_collection.setChecked(false);
                        }
                    }
                });
        this.next_collection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (!next_collection.isChecked()) {
                    all_collection.setChecked(false);
                }

            }
        });
    }

    public void createNavBarItems() {
        //Create Items
        AHBottomNavigationItem calendar = new AHBottomNavigationItem("Calendar", R.drawable.a_default_ico_menu);
        AHBottomNavigationItem materials = new AHBottomNavigationItem("Materials", R.drawable.b_default_ico_menu);
        AHBottomNavigationItem reportProblem = new AHBottomNavigationItem("Other Services", R.drawable.c_default_ico_menu);
        AHBottomNavigationItem wasteMaterial = new AHBottomNavigationItem("More information", R.drawable.d_default_ico_menu);

        // Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(true);
        //Adding Items
        bottomNavigation.addItem(calendar);
        bottomNavigation.addItem(materials);
        bottomNavigation.addItem(reportProblem);
        bottomNavigation.addItem(wasteMaterial);

        //Setting Properties
        bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.orange));
        bottomNavigation.setInactiveColor(getResources().getColor(R.color.black));
        bottomNavigation.setForceTitlesDisplay(true);
        bottomNavigation.setAccentColor(getResources().getColor(R.color.white));
        bottomNavigation.setBehaviorTranslationEnabled(false);

        bottomNavigation.setCurrentItem(0);
//        CoordinatorLayout.LayoutParams params = new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        int height = bottomNavigation.getHeight();
//        params.setMargins(0,0,0,height);
//        linearLayout.setLayoutParams(params);
    }

    @Override
    public void onTabSelected(int position, boolean wasSelected) {
        push_notification_message = pref.getStringValues("notification_message");
        long locationId = pref.getIntValues("item_id");
        if (position == 0) {
            if (locationId != 0) {
                CalendarFragment calendarFragment = new CalendarFragment();
                Bundle args = new Bundle();
                args.putInt("section_id", 3);
                args.putString("notofication_message", push_notification_message);
                calendarFragment.setArguments(args);
                getSupportFragmentManager().beginTransaction().
                        setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                        .replace(R.id.contentID, calendarFragment).commit();

            } else {
                AddressFragment addressFragment = new AddressFragment();
                getSupportFragmentManager().beginTransaction().
                        setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                        .replace(R.id.contentID, addressFragment).commit();
            }

        } else if (position == 1) {
            Material materialFragment = new Material();
            Bundle args = new Bundle();
            args.putLong("section_id", 2);
            materialFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                    .replace(R.id.contentID, materialFragment).commit();

        } else if (position == 2) {
            ReportProblem reportFragment = new ReportProblem();
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                    .replace(R.id.contentID, reportFragment).commit();
        } else if (position == 3) {
            WasteMaterials wasteFragment = new WasteMaterials();
            Bundle args = new Bundle();
            args.putLong("section_id", 1);
            wasteFragment.setArguments(args);
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                    .replace(R.id.contentID, wasteFragment).commit();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?");
        builder.setCancelable(false);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View v) {


        if (v.getId() == btn_proceed.getId()) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

            String suburb_text = base_suburb.getText().toString().trim();
            String address_text = base_street.getText().toString().trim();

            if (address_text.isEmpty() && suburb_text.isEmpty()) {
                Toast toast = Toast.makeText(this,
                        "Please search for your address", Toast.LENGTH_LONG);
                toast.show();
            } else {
                pref.setKeyValues("user_suburb", suburb_text);
                pref.setKeyValues("user_address", address_text);

                if (library.isConnectingToInternet()) {
                    getLocation.getLocationMethod(suburb_text, address_text, base_suburb);
                } else {
                    Toast.makeText(AddressActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }
            }
        }

        if (v.getId() == btn_set_alarm.getId()) {
            Log.d("test", "set button clicked!");
            this.item_id = pref.getIntValues("item_id");
            this.bin_type_id = pref.getIntValues("bin_type_id");
            this.bin_colors = pref.getStringValues("bin_colors");
//            String checkguest = pref.getStringValues("radio");
//
//            if(checkguest.equals("1")){
//                Toast.makeText(this,"Guest cannot set alarm",Toast.LENGTH_SHORT).show();
//            }
            if (item_id != 0) {


                organic_date = pref.getStringValues("item_organic");
                garbage_date = pref.getStringValues("item_garbage");
                recycling_date = pref.getStringValues("item_recycling");
                Date garbage = null;
                Date recycle = null;
                Date organic = null;
                try {
                    garbage = new SimpleDateFormat("yyyy-MM-d").parse(garbage_date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                try {
                    organic = new SimpleDateFormat("yyyy-MM-d").parse(organic_date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }



                if (garbage!=null){
                    garbage_cal = Calendar.getInstance();
                    garbage_cal.setTime(garbage);

//                    Calendar cal = Calendar.getInstance();
//
//                    int compareResultGarbage = garbage_cal.compareTo(cal);
//                    if(compareResultGarbage < 0){
//                        garbage_cal.add(Calendar.DAY_OF_MONTH, 7);
//                    }
                }
                if (organic!=null){
                    organic_cal = Calendar.getInstance();
                    organic_cal.setTime(organic);

//                    Calendar cal = Calendar.getInstance();
//                    int compareResultOrganic = organic_cal.compareTo(cal);
//                    if(compareResultOrganic < 0){
//                        organic_cal.add(Calendar.DAY_OF_MONTH, 14);
//                    }
                }

                // retrieve the attachment dates from the shared preferences
                try {

                    date_list = (ArrayList<Calendar>) ObjectSerializer.deserialize(pref
                            .getStringValues(
                                    "Calendar_Date_List"
                            ));

                    for (int i = 0; i < date_list.size(); i++) {

                        Log.d(LOGTAG, "Result==" + date_list.get(i));
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Calendar date = date_list.get(0);
                Calendar cal = Calendar.getInstance();


                while(garbage_cal.getTimeInMillis()<cal.getTimeInMillis())
                {
                    garbage_cal.add(Calendar.DAY_OF_MONTH, 7);
                }

                while(organic_cal.getTimeInMillis()<cal.getTimeInMillis())
                {
                    organic_cal.add(Calendar.DAY_OF_MONTH, 7);
                }

               /* while (garbage_cal.get(Calendar.DAY_OF_MONTH) <= cal.get(Calendar.DAY_OF_MONTH)) {
                    garbage_cal.add(Calendar.DAY_OF_MONTH, 7);
                }*/

                if (garbage_cal.get(Calendar.DAY_OF_MONTH) - 1 == cal.get(Calendar.DAY_OF_MONTH)) {
                    if (cal.get(Calendar.HOUR_OF_DAY) >= 18 && cal.get(Calendar.MINUTE) > 0
                            && cal.get(Calendar.SECOND) > 0) {
                        garbage_cal.add(Calendar.DAY_OF_MONTH, 7);
                    }
                }
                if (organic_cal.get(Calendar.DAY_OF_MONTH) - 1 == cal.get(Calendar.DAY_OF_MONTH)) {
                    if (cal.get(Calendar.HOUR_OF_DAY) >= 18 && cal.get(Calendar.MINUTE) > 0
                            && cal.get(Calendar.SECOND) > 0) {
                        organic_cal.add(Calendar.DAY_OF_MONTH, 7);
                    }
                }

                if (garbage_cal.get(Calendar.MONTH) - 1 == cal.get(Calendar.MONTH) && (garbage_cal.get(Calendar.DAY_OF_MONTH) == 1) && (cal.getActualMaximum(Calendar.DATE) == cal.get(Calendar.DAY_OF_MONTH))) {
                    System.out.print("prajit minutes" + cal.get(Calendar.MINUTE));
                    if (cal.get(Calendar.HOUR_OF_DAY) >= 18 && cal.get(Calendar.MINUTE) > 0) {
                        garbage_cal.add(Calendar.DAY_OF_MONTH, 7);
                    }}
                if (organic_cal.get(Calendar.MONTH) - 1 == cal.get(Calendar.MONTH) && (garbage_cal.get(Calendar.DAY_OF_MONTH) == 1) && (cal.getActualMaximum(Calendar.DATE) == cal.get(Calendar.DAY_OF_MONTH))) {
                    System.out.print("prajit minutes" + cal.get(Calendar.MINUTE));
                    if (cal.get(Calendar.HOUR_OF_DAY) >= 18 && cal.get(Calendar.MINUTE) > 0) {
                        organic_cal.add(Calendar.DAY_OF_MONTH, 7);
                    }}


                if (all_collection.isChecked()) {
                    if (!date_list.isEmpty()) {


                        Intent myIntent = new Intent(this,
                                AlarmNotificationReceiver.class);
                        PendingIntent pendingIntent = PendingIntent
                                .getBroadcast(this, 0, myIntent,
                                        0);

                        Intent organics_intent = new Intent(this,
                                AlarmNotificationReceiver.class);
                        PendingIntent organics_pending_intent = PendingIntent
                                .getBroadcast(this, 23, organics_intent,
                                        0);

                        // Get the AlarmManager service
                        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
                        AlarmManager am_organic = (AlarmManager) getSystemService(ALARM_SERVICE);

                        garbage_cal.add(Calendar.DATE, -1);
                        garbage_cal.set(Calendar.HOUR_OF_DAY, 18);
                        garbage_cal.set(Calendar.MINUTE, 0);
                        garbage_cal.set(Calendar.SECOND, 0);


                        organic_cal.add(Calendar.DATE, -1);
                        organic_cal.set(Calendar.HOUR_OF_DAY, 18);
                        organic_cal.set(Calendar.MINUTE, 0);
                        organic_cal.set(Calendar.SECOND, 0);

                        if (garbage_cal.get(Calendar.DAY_OF_WEEK)!=organic_cal.get(Calendar.DAY_OF_WEEK)) {
                            am_organic.setRepeating(AlarmManager.RTC_WAKEUP,
                                    organic_cal.getTimeInMillis(), (604800000 * 4),
                                    organics_pending_intent);
                        }

                        am.setRepeating(AlarmManager.RTC_WAKEUP,
                                garbage_cal.getTimeInMillis(), 604800000,
                                pendingIntent);
                        // set repeating every 1
                        // week



                        if (pref != null) {
                            pref.setKeyValues("check_flag_all", 1);
                        }
                    } else {
                        all_collection.setChecked(false);
                        next_collection.setChecked(false);
                        showmessage("There are no collection dates on this month.");
                    }

                    next_collection.setChecked(true);
                    if (pref != null) {
                        pref.setKeyValues("check_flag_next", 1);
                    }

                } else {
                    if (pref != null) {
                        pref.setKeyValues("check_flag_all", 0);
                    }
                }

                if (!all_collection.isChecked()) {

                    if (next_collection.isChecked()) {
                        Log.d(LOGTAG, "Inside next_collection.isChecked()");
                        if (!date_list.isEmpty()) {
//                                Calendar date = date_list.get(0);

                            Intent myIntent = new Intent(this,
                                    AlarmNotificationReceiver.class);
                            PendingIntent pendingIntent = PendingIntent
                                    .getBroadcast(this, 0,
                                            myIntent, 0);

                            // Get the AlarmManager service
                            AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);

                            date.add(Calendar.DATE, -1);
                            date.set(Calendar.HOUR_OF_DAY, 18);
                            date.set(Calendar.MINUTE, 0);
                            date.set(Calendar.SECOND, 0);

                            am.set(AlarmManager.RTC,
                                    date.getTimeInMillis(), pendingIntent);

                            if (pref != null) {
                                pref.setKeyValues("check_flag_next", 1);
                            }
                        } else {
                            showNoCollectionMessage();
                        }
                    } else {
                        if (pref != null) {
                            pref.setKeyValues("check_flag_next", 0);
                        }
                    }
                }

                if (all_collection.isChecked()
                        || next_collection.isChecked()) {
                    if (!date_list.isEmpty()) {
                        displayMessage();
                    }
                }

                if (!all_collection.isChecked()
                        && !next_collection.isChecked()) {
                    Toast.makeText(this, "Please set a time to be reminded the day before your collection",
                            Toast.LENGTH_LONG).show();
                }

            } else {
                showMessage();
            }
            // }

            // scheduleClient.doUnbindService();

        }

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
//        String suburb_txt = pref.getStringValues("Selected suburb");
//        if (this.base_street.hasFocus()) {
//            if (this.library.isConnectingToInternet()) {
//                String suburb_editTxt = this.base_suburb.getText().toString();
//                if (!suburb_editTxt.isEmpty()) {
//                    suburb_editTxt = toTitleCase(suburb_editTxt);
//                }
//                if (this.suburb_list.contains(suburb_editTxt)) {
//                    if (!this.base_street.getText().toString().equals("0") || !suburb_editTxt.equals(suburb_txt)) {
//                        if (this.library.isConnectingToInternet()) {
//                            ArrayList<String> addressSuggestion = new ArrayList<String>();
//                            addressSuggestion = this.getAddressAuto.getAutoStreets(suburb_editTxt);
//                            adapter = new AutocompleteArrayAdapter(this, addressSuggestion);
//                            this.base_street.setAdapter(adapter);
//                            this.base_street.setThreshold(1);
//                        } else {
//                            Toast toast = Toast.makeText(this, "No internet connection!", Toast.LENGTH_SHORT);
//                            toast.show();
//                        }
//                        pref.setKeyValues("Selected suburb", suburb_editTxt);
////                        this.base_street.setText("");
//                    }
//                } else {
//                    adapter = new AutocompleteArrayAdapter(this, null);
//                    this.base_street.setAdapter(adapter);
//                    pref.setKeyValues("Selected suburb", "");
//                }
//            } else {
//                Toast toast = Toast.makeText(this, "No internet connection!", Toast.LENGTH_SHORT);
//                toast.show();
//            }
//        }


    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    public void closeDrawer() {
        drawer.closeDrawers();
    }



    private void showmessage(String message) {
        // TODO Auto-generated method stub

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle("Message");

        builder.setNeutralButton("ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
            }
        });

        android.app.AlertDialog box = builder.create();
        box.show();

    }

    public void showNoCollectionMessage() {
        showmessage("There are no collection dates on this month.");
        // Toast toast = Toast.makeText(this,
        // "There are no collection dates on this month.", Toast.LENGTH_SHORT);
        // toast.show();
    }

    public void displayMessage() {
        showmessage("Reminder set up successfully");
    }

    public void showMessage() {
        Toast toast = Toast.makeText(this,
                "You need to first set the location.", Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    //    /**
//     * Show or hide the bottom navigation with animation
//     */
//    public void showOrHideBottomNavigation(boolean show) {
//        if (show) {
//            bottomNavigation.restoreBottomNavigation(true);
//        } else {
//            bottomNavigation.hideBottomNavigation(true);
//        }
//    }

}
