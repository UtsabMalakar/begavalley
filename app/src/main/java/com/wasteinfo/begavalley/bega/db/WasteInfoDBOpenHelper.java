package com.wasteinfo.begavalley.bega.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class WasteInfoDBOpenHelper extends SQLiteOpenHelper {

	private static final String LOGTAG = "WasteInfoVal";

	private static final String DATABASE_NAME = "wasteinfo.db";
	private static final int DATABASE_VERSION = 2;

	// Default table location

	public static final String TABLE_LOCATION = "location";

	public static final String COLUMN_ITEM_ID = "item_id";
	public static final String COLUMN_UNIT_NUMBER = "unit_number";
	public static final String COLUMN_HOUSE_NUMBER = "house_number";
	public static final String COLUMN_STREET_NAME = "street_name";
	public static final String COLUMN_STREET_TYPE = "street_type";
	public static final String COLUMN_SUBURB = "suburb";
	public static final String COLUMN_POSTCODE = "postcode";
	public static final String COLUMN_COLLECTION_DAY = "collection_day";
	public static final String COLUMN_BIN_TYPE = "bin_type";
	public static final String COLUMN_DWELLING = "dwelling";
	public static final String COLUMN_DATE_TIME = "date_time";
	public static final String COLUMN_BIN_COLORS = "bin_colors";
	public static final String COLUMN_COLLECTION_FREQUENCIES = "collection_frequencies";
	public static final String COLUMN_AREA = "area";
	public static final String COLUMN_ITEM_GARBAGE_DATE = "garbage_date";
	public static final String COLUMN_ITEM_ORGANIC_DATE = "organic_date";
	public static final String COLUMN_ITEM_RECYCLING_DATE = "recycling_date";

	private static final String TABLE_LOCATION_CREATE = "CREATE TABLE "
			+ TABLE_LOCATION + " (" + COLUMN_ITEM_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_UNIT_NUMBER
			+ " INTEGER, " + COLUMN_HOUSE_NUMBER + " VARCHAR, "
			+ COLUMN_STREET_NAME + " VARCHAR, " + COLUMN_STREET_TYPE
			+ " VARCHAR, " + COLUMN_SUBURB + " VARCHAR, " + COLUMN_POSTCODE
			+ " INTEGER, " + COLUMN_COLLECTION_DAY + " VARCHAR, "
			+ COLUMN_BIN_TYPE + " INTEGER, " + COLUMN_DWELLING + " VARCHAR, "
			+ COLUMN_DATE_TIME + " VARCHAR, " + COLUMN_BIN_COLORS
			+ " VARCHAR, " + COLUMN_COLLECTION_FREQUENCIES + " VARCHAR, "
			+ COLUMN_AREA + " VARCHAR, " + COLUMN_ITEM_GARBAGE_DATE
			+ " VARCHAR, " + COLUMN_ITEM_ORGANIC_DATE + " VARCHAR, "
			+ COLUMN_ITEM_RECYCLING_DATE + " VARCHAR" + ")";

	// Default table waste_info_reminder

	public static final String TABLE_NAME_WASTE_INFO_REMINDER = "waste_info_reminder";
	public static final String COL_ID_WASTE_INFO_REMINDER = "reminders_id";
	public static final int NUM_COL_ID_WASTE_INFO_REMINDER = 0;
	public static final String COL_DATE_WASTE_INFO_REMINDER = "reminders_date";
	public static final int NUM_COL_DATE_WASTE_INFO_REMINDER = 1;

	private static final String CREATE_DB_WASTE_INFO_REMINDER = "CREATE TABLE "
			+ TABLE_NAME_WASTE_INFO_REMINDER + " ("
			+ COL_ID_WASTE_INFO_REMINDER
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COL_DATE_WASTE_INFO_REMINDER + " DATE " + ");";

	// Default table waste_info_page

	public static final String TABLE_NAME_WASTE_INFO_PAGE = "waste_info_page";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_PAGE_TITLE = "page_title";
	public static final String COLUMN_PAGE_DESCRIPTION = "page_description";
	public static final String COLUMN_PAGE_CONTENT = "page_content";
	public static final String COLUMN_PAGE_IMAGE = "page_image";
	public static final String COLUMN_SECTION_ID = "section_id";
	public static final String COLUMN_TYPE_OF_CALENDAR = "type_of_calendar";
	public static final String COLUMN_LATITUDE = "latitude";
	public static final String COLUMN_LONGITUDE = "longitude";
	public static final String COLUMN_PAGE_ORDER = "page_order";
	public static final String COLUMN_PAGE_STATUS = "page_status";
	public static final String COLUMN_ALLOWED = "allowed";
	public static final String COLUMN_NOT_ALLOWED = "not_allowed";
	public static final String COLUMN_BIN_TYPE_ID = "bin_type_id";
	public static final String COLUMN_BIN_COLOR_ID = "bin_color_id";

	private static final String CREATE_DB_WASTE_INFO_PAGE = "CREATE TABLE "
			+ TABLE_NAME_WASTE_INFO_PAGE + " (" + COLUMN_ID
			+ " INTEGER PRIMARY KEY, " + COLUMN_PAGE_TITLE + " VARCHAR, "
			+ COLUMN_PAGE_DESCRIPTION + " VARCHAR, " + COLUMN_PAGE_CONTENT
			+ " TEXT, " + COLUMN_PAGE_IMAGE + " TEXT, " + COLUMN_SECTION_ID
			+ " INTEGER, " + COLUMN_TYPE_OF_CALENDAR + " INTEGER, "
			+ COLUMN_LATITUDE + " VARCHAR(255), " + COLUMN_LONGITUDE
			+ " VARCHAR(255), " + COLUMN_PAGE_ORDER + " INTEGER, "
			+ COLUMN_PAGE_STATUS + " INTEGER, " + COLUMN_ALLOWED + " VARCHAR, "
			+ COLUMN_NOT_ALLOWED + " VARCHAR, " + COLUMN_BIN_TYPE_ID
			+ " INTEGER, " + COLUMN_BIN_COLOR_ID + " INTEGER " + ");";

	// Default table section
	public static final String TABLE_NAME_WASTE_INFO_SECTION = "waste_info_section";
	public static final String COLUMN_SECTION_SECTION_ID = "id";
	public static final String COLUMN_SECTION_TITLE = "title";
	public static final String COLUMN_SECTION_PARENT_ID = "parent_id";
	public static final String COLUMN_SECTION_STATUS = "status";

	private static final String CREATE_DB_WASTE_INFO_SECTION = "CREATE TABLE "
			+ TABLE_NAME_WASTE_INFO_SECTION + " (" + COLUMN_SECTION_SECTION_ID
			+ " INTEGER PRIMARY KEY, " + COLUMN_SECTION_TITLE + " VARCHAR, "
			+ COLUMN_SECTION_PARENT_ID + " INTEGER, " + COLUMN_SECTION_STATUS
			+ " TINYINT " + ");";

	// Default table problem_type
	public static final String TABLE_NAME_WASTE_INFO_PROBLEM_TYPE = "waste_info_problem_type";
	public static final String COLUMN_PROBLEM_TYPE_ID = "id";
	public static final String COLUMN_PROBLEM_TYPE_TITLE = "title";
	public static final String COLUMN_PROBLEM_TYPE_EMAIL = "email";

	// private static final String CREATE_DB_WASTE_INFO_PROBLEM_TYPE =
	// "CREATE TABLE "
	// + TABLE_NAME_WASTE_INFO_PROBLEM_TYPE
	// + " ("
	// + COLUMN_PROBLEM_TYPE_ID
	// + " INTEGER PRIMARY KEY AUTOINCREMENT, "
	// + COLUMN_PROBLEM_TYPE_TITLE + " VARCHAR " + ");";
	private static final String CREATE_DB_WASTE_INFO_PROBLEM_TYPE = "CREATE TABLE "
			+ TABLE_NAME_WASTE_INFO_PROBLEM_TYPE
			+ " ("
			+ COLUMN_PROBLEM_TYPE_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_PROBLEM_TYPE_TITLE
			+ " VARCHAR, "
			+ COLUMN_PROBLEM_TYPE_EMAIL + " VARCHAR " + ");";

	public WasteInfoDBOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TABLE_LOCATION_CREATE);
		db.execSQL(CREATE_DB_WASTE_INFO_REMINDER);
		db.execSQL(CREATE_DB_WASTE_INFO_PAGE);
		db.execSQL(CREATE_DB_WASTE_INFO_SECTION);
		db.execSQL(CREATE_DB_WASTE_INFO_PROBLEM_TYPE);
		Log.i(LOGTAG, "The tables have been created!");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_WASTE_INFO_REMINDER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_WASTE_INFO_PAGE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_WASTE_INFO_SECTION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_WASTE_INFO_PROBLEM_TYPE);

		
		// Create tables again
		onCreate(db);

	}

}
