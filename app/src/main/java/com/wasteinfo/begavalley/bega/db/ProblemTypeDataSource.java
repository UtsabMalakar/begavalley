package com.wasteinfo.begavalley.bega.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.wasteinfo.begavalley.bega.RetrofitModel.ProblemType;

import java.util.ArrayList;
import java.util.List;

public class ProblemTypeDataSource {

	
private static final String LOGTAG = "WasteInfoVal";
	
	SQLiteOpenHelper dbhelper;
	SQLiteDatabase database;
	
	public ProblemTypeDataSource(Context context) {
		dbhelper = new WasteInfoDBOpenHelper(context); 
		
	}

	public void open(){
		database = dbhelper.getWritableDatabase();
	}
	
	public void close(){
		dbhelper.close();
	}
	
	public void insertOrUpdateProblemType(ProblemType problemType){
		int problem_type_id=problemType.getId();
		
		Cursor c=database.rawQuery("SELECT * FROM "+WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PROBLEM_TYPE+" WHERE "+WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_ID+"="+problem_type_id,null);
	
		if(c.getCount() == 0){
			//if the row is not present,then insert the row
			ContentValues values = new ContentValues();
			values.put(WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_ID, problemType.getId());
			values.put(WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_TITLE, problemType.getTitle());
			values.put(WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_EMAIL, problemType.getEmail());
			
			
			database.insert(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PROBLEM_TYPE, null, values);
		}else{
			//else update the row
			ContentValues updatedValues =new ContentValues();
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_TITLE, problemType.getTitle());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_EMAIL, problemType.getEmail());
			
			database.update(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PROBLEM_TYPE,updatedValues, WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_ID+"="+ problem_type_id ,null );
		}
	}
	
	public List<ProblemType> getAllProblemTypes(){
		Cursor c_problemTypes=this.database.query(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PROBLEM_TYPE, new String[]{
				WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_ID,
				WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_TITLE,
				WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_EMAIL,
				},null, null, null, null, null);
		
		if(c_problemTypes.getCount() != 0){
			List<ProblemType> problemTypes = new ArrayList<ProblemType>();
			while(c_problemTypes.moveToNext()){
			 	ProblemType problemType=new ProblemType();
                problemType.setId(c_problemTypes.getInt(c_problemTypes.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_ID)));
                problemType.setTitle(c_problemTypes.getString(c_problemTypes.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_TITLE)));
				problemType.setEmail(c_problemTypes.getString(c_problemTypes.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_EMAIL)));
                if(problemType != null)
                    problemTypes.add(problemType);
            }
			c_problemTypes.close();
            return problemTypes;
            
		}
		c_problemTypes.close();
		return null;
	}
		
	}
//	
//	public void deleteProblemType(String problem_type_title){
//		try{
//			this.database.delete(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PROBLEM_TYPE, WasteInfoDBOpenHelper.COLUMN_PROBLEM_TYPE_TITLE+ "=?",new String[] { String.valueOf(problem_type_title)});
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//	}


