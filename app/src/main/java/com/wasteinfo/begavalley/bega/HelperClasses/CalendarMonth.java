package com.wasteinfo.begavalley.bega.HelperClasses;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.db.LocationDataSource;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@SuppressLint("NewApi")
public class CalendarMonth implements Serializable {

	private ArrayList<CalendarDate> month_date;
	
	private int month;
	private int year;
	private Context context;
//	private SharedPreferences preferences;
	private LocationDataSource datasource;

	SharedPref pref;
	
	private static final String LOGTAG = "WasteInfoVal";

	public CalendarMonth() {
		this.month = 0;
		this.year = 0;
		this.context = null;
		datasource = new LocationDataSource(this.context);
		pref = new SharedPref(this.context);

		
		this.month_date = generate(month, year);
	}

	public CalendarMonth(int month, int year) {
		this.month = month;
		this.year = year;
		this.context = null;
		datasource = new LocationDataSource(this.context);
		pref = new SharedPref(this.context);
		
		this.month_date = generate(month, year);
	}

	public CalendarMonth(int month, int year, Context context) {
		this.month = month;
		this.year = year;
		this.context = context;
		datasource = new LocationDataSource(this.context);
		pref = new SharedPref(this.context);

		
		this.month_date = generate(month, year);
	}

	public ArrayList<CalendarDate> getMonth_date() {
		//Check if the month has 6 weeks
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR,this.year);
		cal.set(Calendar.MONTH,this.month);
        cal.set(Calendar.DAY_OF_MONTH,1);
        int maxWeeknumber = cal.getActualMaximum(Calendar.WEEK_OF_MONTH);
//		cal.set(this.year, this.month, 1);
//		int maxWeeknumber = cal.getActualMaximum(Calendar.WEEK_OF_MONTH);
		ArrayList<Integer> positionList = new ArrayList<Integer>();
		/*if(maxWeeknumber == 6){
//			Calendar.WEEK_OF_MONTH
			// rearrange the calendar arraylist
			ArrayList<CalendarDate> tempList=new ArrayList<CalendarDate>();
			for(int i=0; i < month_date.toArray().length ;i++){
				Calendar c = month_date.get(i).getDate();
				if(c.get(Calendar.WEEK_OF_MONTH) == 6 && c.get(Calendar.MONTH) == this.month){
					tempList.add(month_date.get(i));
					positionList.add(i);
					Log.d(LOGTAG,"6th Week Date="+month_date.get(i).getDate());
				}
			}
			
			int length = month_date.toArray().length - 1;
			
			for(int i=length; i >= positionList.get(0); i--){
				try{
					month_date.remove(i);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(tempList.size() > 0){
				
				int index = tempList.size()-1;
				while(month_date.get(index).getDate().get(Calendar.DATE) != 1){
					month_date.set(index,null);
					index++;
				}
				
				for(int i = 0; i<tempList.size(); i++){
					month_date.set(i,tempList.get(i));
				}
			}
			
		}*/
		
		return month_date;
		
	}

	public void setMonth_date(ArrayList<CalendarDate> month_date) {
		this.month_date = month_date;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
		this.month_date = generate(month, this.year);
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
		this.month_date = generate(this.month, year);
	}

	private ArrayList<CalendarDate> generate(int month, int year) {
		int number_days = getNumberDaysOfMonth(month, year);
		int first_day_month = getFirstDayOfMonth(month, year);
		int last_day_month = getLastDayOfMonth(month, year);
		
		Log.d(LOGTAG,"Number of days of month="+number_days);

		ArrayList<CalendarDate> calendar = new ArrayList<CalendarDate>();

		// Check if the first day of the month is Sunday
		Log.e("First day of month", "" + first_day_month);

		
		
		if (first_day_month > 1) {
						
			if(month == 0){
				month = 12;
				year=year-1;
			}
			
			int number_days_last_month = getNumberDaysOfMonth(month - 1, year);
			Log.d(LOGTAG,"Number of days of last month==="+number_days_last_month);
			int number_get = first_day_month;
			
			for(int i=(number_days_last_month - number_get)+2 ; i <= number_days_last_month; i++){
				int mnth =  month-1;
				Calendar date = Calendar.getInstance();
				date.set(Calendar.YEAR,year);
				date.set(Calendar.MONTH,mnth);
				date.set(Calendar.DATE,i);


				ArrayList<Object> list = treatment_attachment(date);

				calendar.add(new CalendarDate(date, list));
			}
		}
		
		if(month == 12){
			month = 0;
			year = year + 1;
		}

		for (int i = 1; i <= number_days; i++) {
			Calendar date=Calendar.getInstance();
			date.set(Calendar.YEAR,year);
			date.set(Calendar.MONTH,month);
			date.set(Calendar.DATE,i);

			ArrayList<Object> list = treatment_attachment(date);
			calendar.add(new CalendarDate(date, list));
		}

		
		
		// Check if the last day of the month is Saturday
		if (last_day_month < 8) {
			int number_get = 8 - last_day_month;
			// Adding new month
			
			if(month == 11){
				month = -1;
				year = year + 1;
				
			}
			
			for (int i = 1; i < number_get; i++) {
				Calendar date=Calendar.getInstance();
				date.set(Calendar.YEAR,year);
				date.set(Calendar.MONTH,month + 1);
				date.set(Calendar.DATE,i);
				


				ArrayList<Object> list = treatment_attachment(date);
				calendar.add(new CalendarDate(date, list));
				//calendar.add(new CalendarDate(date));
			}
		}
		
		return calendar;

	}

	private int getNumberDaysOfMonth(int month, int year) {
		Calendar calendar = Calendar.getInstance();
		
	    calendar.set(Calendar.YEAR,year);
	    calendar.set(Calendar.MONTH,month);
	    calendar.set(Calendar.DATE, 1);

	    int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	    return days;
	}

	private int getFirstDayOfMonth(int month, int year) {
		Calendar c=Calendar.getInstance();
		c.set(Calendar.YEAR,year);
		c.set(Calendar.MONTH,month);
	    c.set(Calendar.DAY_OF_MONTH, 1);

		return c.get(Calendar.DAY_OF_WEEK);
		}

	private int getLastDayOfMonth(int month, int year) {
		int last = getNumberDaysOfMonth(month, year);
		Calendar c=Calendar.getInstance();
		c.set(Calendar.YEAR,year);
		c.set(Calendar.MONTH,month);
	    c.set(Calendar.DAY_OF_MONTH,last);
	    

	    return c.get(Calendar.DAY_OF_WEEK);
	}



	
	
	private ArrayList<Object> treatment_attachment(Calendar date) {
		ArrayList<Object> list = new ArrayList<Object>();
		//AddressList location = new AddressList();
		
//		preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
		int id=pref.getIntValues("item_id");
		String Area = pref.getStringValues("Area");
		Log.d(LOGTAG,"treatment");
		Log.d(LOGTAG,"VAL=="+id);
		
		datasource.open();

		if (this.context != null) {

			String extractedOrganicDate = datasource.getOrganicDateByItemId(id);
			Log.d("testorganic","Extracted organic date="+extractedOrganicDate);
			if(extractedOrganicDate!=null&&!extractedOrganicDate.isEmpty()){
			try {
				Calendar tmp;
				
				Date organic = new SimpleDateFormat("yyyy-MM-d").parse(extractedOrganicDate);
				Calendar cal = Calendar.getInstance();
				cal.setTime(organic);
				cal.add(Calendar.DATE, -28);

				
				while (true) {
					
					cal.add(Calendar.DATE, 28);
					tmp = cal;
					if (tmp.get(Calendar.DATE) == date.get(Calendar.DATE)
							&& tmp.get(Calendar.MONTH) == date.get(Calendar.MONTH)
							&& tmp.get(Calendar.YEAR) == date.get(Calendar.YEAR)) {
						list.add("organic");
						break;
					}

					if (tmp.get(Calendar.DATE) >= date.get(Calendar.DATE)
							&& tmp.get(Calendar.MONTH) >= date.get(Calendar.MONTH)
							&& tmp.get(Calendar.YEAR) >= date.get(Calendar.YEAR))
						break;
				}

			} catch (ParseException e) {
				e.printStackTrace();
			}
			}
			
			
			String extractedRecyclingDate = datasource.getRecyclingDateByItemId(id);
			Log.d("testrecyledate","Extracted recycyling date="+extractedRecyclingDate);
			if(extractedRecyclingDate!=null&&!extractedRecyclingDate.isEmpty()){
			try {
				Calendar tmp;
				Date recycling = new SimpleDateFormat("yyyy-MM-d").parse(extractedRecyclingDate);
				Calendar cal = Calendar.getInstance();
				cal.setTime(recycling);
				cal.add(Calendar.DATE, -14);
				
				while (true) {
					cal.add(Calendar.DATE, 14);
					tmp = cal;
					if (tmp.get(Calendar.DATE) == date.get(Calendar.DATE)
							&& tmp.get(Calendar.MONTH) == date.get(Calendar.MONTH)
							&& tmp.get(Calendar.YEAR) == date.get(Calendar.YEAR)) {
						list.add("recycling");
						break;
					}

					if (tmp.get(Calendar.DATE) >= date.get(Calendar.DATE)
							&& tmp.get(Calendar.MONTH) >= date.get(Calendar.MONTH)
							&& tmp.get(Calendar.YEAR) >= date.get(Calendar.YEAR))
						break;
				}

			} catch (ParseException e) {
				e.printStackTrace();
			}
			}
			
			String extractedGarbageDate = datasource.getGarbageDateByItemId(id);
			Log.d("testgarbage","Extracted garbage date="+extractedGarbageDate);
			
			if(extractedGarbageDate!=null&&!extractedGarbageDate.isEmpty()){
			try {
//				SharedPreferences pref = this.context.getSharedPreferences("MyPref", 0);
				
				String collection_check = pref.getStringValues("user_collection");
				
				Calendar tmp;
				Date garbage = new SimpleDateFormat("yyyy-MM-d")
						.parse(extractedGarbageDate);
				Calendar cal = Calendar.getInstance();
				cal.setTime(garbage);
				
				
				if(collection_check.equals("1")){
					cal.add(Calendar.DATE, -7);
					
					while (true) {
						cal.add(Calendar.DATE, 7);
						tmp = cal;
						if (tmp.get(Calendar.DATE) == date.get(Calendar.DATE)
								&& tmp.get(Calendar.MONTH) == date.get(Calendar.MONTH)
								&& tmp.get(Calendar.YEAR) == date.get(Calendar.YEAR)) {
							list.add("garbage");
							break;
						}

						if (tmp.get(Calendar.DATE) >= date.get(Calendar.DATE)
								&& tmp.get(Calendar.MONTH) >= date.get(Calendar.MONTH)
								&& tmp.get(Calendar.YEAR) >= date.get(Calendar.YEAR))
							break;
					}
				}else{
					cal.add(Calendar.DATE, -7); //14
				
				while (true) {
					cal.add(Calendar.DATE, 7);
					tmp = cal;
					if (tmp.get(Calendar.DATE) == date.get(Calendar.DATE)
							&& tmp.get(Calendar.MONTH) == date.get(Calendar.MONTH)
							&& tmp.get(Calendar.YEAR) == date.get(Calendar.YEAR)) {
						list.add("garbage");
						break;
					}

					if (tmp.get(Calendar.DATE) >= date.get(Calendar.DATE)
							&& tmp.get(Calendar.MONTH) >= date.get(Calendar.MONTH)
							&& tmp.get(Calendar.YEAR) >= date.get(Calendar.YEAR))
						break;
					}
				}

			} catch (ParseException e) {
				e.printStackTrace();
			}
			}
		
			
		}
		Log.d(LOGTAG,"Calendar="+list);
		
		//datasource.close();
		return list;

	}

}
