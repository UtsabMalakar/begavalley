package com.wasteinfo.begavalley.bega.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Calendar;

public class ReminderDataSource {

private static final String LOGTAG = "WasteInfoVal";
	
	SQLiteOpenHelper dbhelper;
	SQLiteDatabase database;
	
	public ReminderDataSource(Context context){
		dbhelper = new WasteInfoDBOpenHelper(context);
	}
	
	public void open(){
		Log.d(LOGTAG,"Database opened!");
		database = dbhelper.getWritableDatabase();
	}
	
	public void close(){
		Log.d(LOGTAG,"Database closed!");
		dbhelper.close();
	}
	
	public long setReminder(Calendar date){
		long id = 0;
		ContentValues values = new ContentValues();
        values.put(WasteInfoDBOpenHelper.COL_DATE_WASTE_INFO_REMINDER, date.get(Calendar.YEAR)+"-"+date.get(Calendar.MONTH)+"-"+date.get(Calendar.DATE));
        id = database.insert(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_REMINDER, null, values);
        Log.d(LOGTAG,"Reminder Set with Id=== "+id);
        return id;
	}

	public long getReminder(Calendar date){
		long id = 0;
		 Cursor c_reminder = this.database.query( WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_REMINDER, new String[] {
	                WasteInfoDBOpenHelper.COL_ID_WASTE_INFO_REMINDER,
	                WasteInfoDBOpenHelper.COL_DATE_WASTE_INFO_REMINDER
	        }
	                , WasteInfoDBOpenHelper.COL_DATE_WASTE_INFO_REMINDER + " = '" +date.get(Calendar.YEAR)+"-"+date.get(Calendar.MONTH)+"-"+date.get(Calendar.DATE)+"'", null, null, null, null);

	        if(c_reminder.getCount() != 0){
	            c_reminder.moveToNext();
	            id = c_reminder.getInt(WasteInfoDBOpenHelper.NUM_COL_ID_WASTE_INFO_REMINDER);
	        }
	        return id;
	}
	
	public void deleteReminder(Calendar date){
        this.database.delete(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_REMINDER, WasteInfoDBOpenHelper.COL_DATE_WASTE_INFO_REMINDER + " = '" +date.get(Calendar.YEAR)+"-"+date.get(Calendar.MONTH)+"-"+date.get(Calendar.DATE)+"'", null);
    }

	
}
