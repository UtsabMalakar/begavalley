package com.wasteinfo.begavalley.bega.Alarm;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.wasteinfo.begavalley.bega.WebServiceTasks.UpdateDB;

public class AlarmReceiver extends BroadcastReceiver {
    private static final String LOGTAG = "WasteInfoVal";
    private Library library;
    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            //Toast.makeText(context, "test!", Toast.LENGTH_SHORT).show();
            Log.d(LOGTAG, "Inside AlarmReceiver");
            SharedPreferences settings = context.getSharedPreferences("PREF_TAG_TIMESTAMP", 0);
            String ts = settings.getString("currentTimestamp", "");

            this.context = context;
            this.library = new Library(this.context);

            if (this.library.isConnectingToInternet()) {
                UpdateDB runner = new UpdateDB(context);
                runner.getUpdated();

//                Toast.makeText(this.context, "Updated", Toast.LENGTH_SHORT).show();
            } else {
                Toast toast = Toast.makeText(this.context, "No internet connection!", Toast.LENGTH_SHORT);
                toast.show();
            }
        } catch (Exception e) {
            //Toast.makeText(context, "There was an error somewhere, but we still received an alarm", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

}
