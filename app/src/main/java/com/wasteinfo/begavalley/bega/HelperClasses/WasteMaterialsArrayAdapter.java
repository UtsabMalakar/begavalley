package com.wasteinfo.begavalley.bega.HelperClasses;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.RetrofitModel.Section;
import com.wasteinfo.begavalley.bega.RetrofitModel.Sections;

import java.util.List;

//import com.applidium.shutterbug.FetchableImageView;


public class WasteMaterialsArrayAdapter extends ArrayAdapter<List> {
	
	private Context context;
    private List<Object> list;
    private Library library;
    

	public WasteMaterialsArrayAdapter(Context context, List list) {
		super(context, R.layout.list_item_waste_materials,list);
		this.context = context;
        this.list = list;
        this.library=new Library(this.context);
        
        
	}
	
	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView;

      //Check the class
        if(list.get(position).getClass().toString().equals(Section.class.toString())){
        	rowView = inflater.inflate(R.layout.list_item_waste_materials, parent, false);
        	TextView value = (TextView) rowView.findViewById(R.id.list_item_waste_value);
        	ImageView imgV= (ImageView) rowView.findViewById(R.id.list_item_waste_image);
            value.setText(((Sections)list.get(position)).getTitle());
            imgV.setImageDrawable(this.context.getResources().getDrawable(R.drawable.arrow_right));
            
          //Treatment font
            value.setTypeface(this.library.getDefaultFont("regular"));
            
        }else{
        	if(((Page)list.get(position)).getPageImage().isEmpty()){
        	rowView = inflater.inflate(R.layout.list_item_waste_materials, parent, false);
            TextView value = (TextView) rowView.findViewById(R.id.list_item_waste_value);
            ImageView binImgView=(ImageView) rowView.findViewById(R.id.list_item_waste_image);
            
            try {
                int bin_color_id=((Page)list.get(position)).getBinColorId();
                if(bin_color_id == 1){
                	binImgView.setImageResource(R.drawable.round_garbage);
                }else if(bin_color_id == 2){
                	binImgView.setImageResource(R.drawable.round_organics);
                }else if(bin_color_id == 3){
                	binImgView.setImageResource(R.drawable.round_recycling);
                }else if(bin_color_id == 4){
                	binImgView.setImageResource(R.drawable.round_paper_recycling);
                }else{
                	binImgView.setVisibility(View.INVISIBLE);
                }
                }catch(Exception e){
                	e.printStackTrace();
                }
    		
            
            value.setText(((Page)list.get(position)).getPageTitle());
          //Treatment font
            value.setTypeface(this.library.getDefaultFont("regular"));
          
        	}else{
        		String imagePath=((Page)list.get(position)).getPageImage();
        		rowView = inflater.inflate(R.layout.list_item_image_waste, parent, false);
        		ImageView left = (ImageView) rowView.findViewById(R.id.list_item_image_waste_image_left);
        		ImageView right = (ImageView) rowView.findViewById(R.id.list_item_image_waste_image_right);
                TextView text_1 = (TextView) rowView.findViewById(R.id.list_item_image_waste_value_1);
                TextView text_2 = (TextView) rowView.findViewById(R.id.list_item_image_waste_value_2);
                
                //DownloadImageTask.getInstance().loadBitmap(imagePath, left,this.context);

                Picasso.with(this.context).load(imagePath).into(left);
                
                //left.setImage(imagePath, this.context.getResources().getDrawable(R.drawable.ic_launcher));
                
                
                 //Treatment text
                text_1.setText(((Page)list.get(position)).getPageTitle());
                text_2.setText(((Page)list.get(position)).getPageDescription());
                
                //Treatment font
                text_1.setTypeface(this.library.getDefaultFont("regular"));
                text_2.setTypeface(this.library.getDefaultFont("regular"));
                
                //Treatment waste
                try {
                int bin_color_id=((Page)list.get(position)).getBinColorId();
                if(bin_color_id == 1){
                	right.setImageResource(R.drawable.round_garbage);
                }else if(bin_color_id == 2){
                	right.setImageResource(R.drawable.round_organics);
                }else if(bin_color_id == 3){
                	right.setImageResource(R.drawable.round_recycling);
                }else if(bin_color_id == 4){
                	right.setImageResource(R.drawable.round_paper_recycling);
                }else{
                	right.setVisibility(View.INVISIBLE);
                }
                }catch(Exception e){
                	e.printStackTrace();
                }
        	}
        	
        }
		return rowView;
	}
	
		
}
