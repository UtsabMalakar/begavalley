package com.wasteinfo.begavalley.bega.WebServiceTasks;

import android.content.Context;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitAPI.MoiraAPI;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.RetrofitModel.Section;
import com.wasteinfo.begavalley.bega.RetrofitModel.Sections;
import com.wasteinfo.begavalley.bega.db.PageDataSource;
import com.wasteinfo.begavalley.bega.db.SectionDataSource;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Test on 6/24/2016.
 */
public class GetSections {

    private Context context;
    private String url;
    private SectionDataSource sectionDataSource;
    private PageDataSource pageDataSource;
    private List<Object> item_list;
    public GetSections(Context context){
        this.context = context;
        this.url = context.getResources().getString(R.string.url);
        this.sectionDataSource = new SectionDataSource(this.context);
        this.pageDataSource = new PageDataSource(this.context);
    }

    public List<Object> getSectionPage(int section_id, int bin_type, List<Object> list){
        this.item_list = list;


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(this.url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();
        MoiraAPI moiraAPI = retrofit.create(MoiraAPI.class);

        Call<Section> SectionPage = moiraAPI.getSection(section_id,bin_type);
        SectionPage.enqueue(new Callback<Section>() {
            @Override
            public void onResponse(Call<Section> call, Response<Section> response) {
                getSections(response);
                getPages(response);



            }

            @Override
            public void onFailure(Call<Section> call, Throwable t) {
                item_list = null;
            }
        });
            return item_list;
    }

    private void getPages(Response<Section> response) {
        Page page;
        List<Page> pages = new ArrayList<Page>();
        pageDataSource.open();

        for (int i = 0; i < response.body().getPages().size(); i++) {
            page = new Page();
            page.setId(response.body().getPages().get(i).getId());
            page.setPageTitle(response.body().getPages().get(i).getPageTitle());
            page.setPageDescription(response.body().getPages().get(i).getPageDescription());
            page.setPageContent(response.body().getPages().get(i).getPageContent());
            page.setPageImage(response.body().getPages().get(i).getPageImage());
            page.setSectionId(response.body().getPages().get(i).getSectionId());
            page.setTypeOfCalendar(response.body().getPages().get(i).getTypeOfCalendar());
            page.setLatitude(response.body().getPages().get(i).getLatitude());
            page.setLongitude(response.body().getPages().get(i).getLongitude());
            page.setOrder(response.body().getPages().get(i).getOrder());
            page.setPageStatus(response.body().getPages().get(i).getPageStatus());
            page.setAllowed(response.body().getPages().get(i).getAllowed());
            page.setNotAllowed(response.body().getPages().get(i).getNotAllowed());
            page.setBinTypeId(response.body().getPages().get(i).getBinTypeId());
            page.setBinColorId(response.body().getPages().get(i).getBinColorId());

            pages.add(page);
        }
        pageDataSource.close();
//        if (pages != null) {
//            for (int i = 0; i < pages.toArray().length; i++) {
//                this.item_list.add(pages.get(i));
//            }
//        }
    }

    private void getSections(Response<Section> response) {
        Sections sections;
        sectionDataSource.open();

        List<Sections> listSections = new ArrayList<Sections>();
        for (int i = 0; i < response.body().getSections().size(); i++) {
            sections = new Sections();
            long id = response.body().getSections().get(i).getId();
            sections.setId(id);
            sections.setTitle(response.body().getSections().get(i).getTitle());
            sections.setStatus(response.body().getSections().get(i).getStatus());
            sections.setSectionId(response.body().getSections().get(i).getSectionId());

            listSections.add(sections);
        }
        sectionDataSource.close();
        if (listSections != null) {
            for (int i = 0; i < listSections.toArray().length; i++) {
                this.item_list.add(listSections.get(i));
            }


        }
    }
}
