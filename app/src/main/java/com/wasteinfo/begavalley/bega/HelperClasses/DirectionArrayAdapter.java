package com.wasteinfo.begavalley.bega.HelperClasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitModel.Step;

import org.jsoup.Jsoup;

import java.util.List;

;

public class DirectionArrayAdapter extends ArrayAdapter<List> {

	private Context context;
	private List<Step> list;
	Library library;
	
	public DirectionArrayAdapter(Context context, List list) {
		super(context, R.layout.list_item_direction,list);
		this.context = context;
        this.list = list;
        this.library=new Library(this.context);
        
        
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView=inflater.inflate(R.layout.list_item_direction, parent, false);
        TextView tv_distance = (TextView) rowView.findViewById(R.id.distance_txt);
        TextView tv_duration = (TextView) rowView.findViewById(R.id.time_txt);
        TextView tv_direction = (TextView) rowView.findViewById(R.id.direction_txt);
        
        tv_distance.setText(((Step)list.get(position)).getDistance());
        tv_duration.setText(((Step)list.get(position)).getDuration());
        String html_content= Jsoup.parse(((Step)list.get(position)).getHtmlInstruction()).text();
        tv_direction.setText(html_content);
        
        tv_distance.setTypeface(this.library.getDefaultFont("regular"));
        tv_duration.setTypeface(this.library.getDefaultFont("regular"));
        tv_direction.setTypeface(this.library.getDefaultFont("regular"));
		return rowView;
	}
	
}
