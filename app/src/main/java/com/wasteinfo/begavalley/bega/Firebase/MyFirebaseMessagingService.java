package com.wasteinfo.begavalley.bega.Firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Activities.MainActivity;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    SharedPref pref;
    String message;

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        pref = new SharedPref(this);

        //Displaying data in log
        //It is optional
//        Log.d(TAG, "From: " + remoteMessage.getFrom());
//        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());

        Map map = remoteMessage.getData();
//       String data = (String) map.get("greetMsg");
//
//       Log.d(TAG, "data: " + data);

        Map msg = remoteMessage.getData();
        String newmsg = (String) msg.get("m");


        try {
            JSONObject m = new JSONObject(newmsg);
            message = m.getString("greetMsg").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        pref.setKeyValues("notification_message",message);
        //Calling method to generate notification
        sendNotification(message);
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification_icon1)
                .setContentTitle("Bega Valley")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
