package com.wasteinfo.begavalley.bega.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.HelperClasses.AutocompleteArrayAdapter;
import com.wasteinfo.begavalley.bega.HelperClasses.KeyboardHelper;
import com.wasteinfo.begavalley.bega.HelperClasses.ObjectSerializer;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.WebServiceTasks.GetLocation;
import com.wasteinfo.begavalley.bega.WebServiceTasks.GetSuburbs;

import java.io.IOException;
import java.util.ArrayList;


public class AddressFragment extends Fragment implements View.OnFocusChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private Button btn_proceed, btn_email;
    private AutoCompleteTextView tv_suburb;
    private AutoCompleteTextView tv_address;
    private EditText et_street_no;
    private RadioButton radio_guest, radio_res;
    private Library library;
    private Context context;

    private GetSuburbs getAddressAuto;
    private GetLocation getLocation;
    private ArrayList<String> suburb_list;
    private ArrayList<String> addressAuto;
    private SharedPref pref;
    private AutocompleteArrayAdapter adapter;
    private TextView tv_label_title;
    private String checkRadio = "2";
    AddressActivity addressActivity;

    private KeyboardHelper kb_helper;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.addressActivity = (AddressActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_address, container, false);

        this.context = container.getContext();

        kb_helper = new KeyboardHelper();
        kb_helper.setupUI(rootview.findViewById(R.id.add_activity), addressActivity);
        kb_helper.setupUI(rootview.findViewById(R.id.LlayoutId), addressActivity);

        this.btn_proceed = (Button) rootview.findViewById(R.id.address_btn_proceed);
        this.tv_address = (AutoCompleteTextView) rootview.findViewById(R.id.address_value);
        this.tv_suburb = (AutoCompleteTextView) rootview.findViewById(R.id.address_suburb);
        this.et_street_no = (EditText) rootview.findViewById(R.id.et_street_no);
//        this.radio_guest = (RadioButton) rootview.findViewById(R.id.radio_guest);
//        this.radio_res = (RadioButton) rootview.findViewById(R.id.radio_res);
//        this.tv_label_title = (TextView) rootview.findViewById(R.id.address_label_title);
        this.btn_email = (Button) rootview.findViewById(R.id.btn_send_email);
        this.library = new Library(this.context);
        this.pref = new SharedPref(this.context);

        this.getAddressAuto = new GetSuburbs(this.context);
        this.getLocation = new GetLocation(this.context);

        this.suburb_list = new ArrayList<String>();
        this.addressAuto = new ArrayList<String>();
        try {
            suburb_list = (ArrayList<String>) ObjectSerializer.deserialize(pref.getStringValues("Suburbs"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        addressActivity.tv_title.setText(this.context.getResources().getString(R.string.app_name));
        addressActivity.toggle.setDrawerIndicatorEnabled(false);
//        addressActivity.toggle.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.a_arrow_left));


        this.adapter = new AutocompleteArrayAdapter(this.context, suburb_list);
        this.tv_suburb.setAdapter(adapter);
        this.tv_suburb.setThreshold(1);


        this.btn_proceed.setTypeface(this.library.getDefaultFont("regular"));
        this.tv_address.setTypeface(this.library.getDefaultFont("regular"));
        this.tv_suburb.setTypeface(this.library.getDefaultFont("regular"));
        this.et_street_no.setTypeface(this.library.getDefaultFont("regular"));
//        this.radio_guest.setTypeface(this.library.getDefaultFont("bold"));
//        this.radio_res.setTypeface(this.library.getDefaultFont("bold"));

//        this.tv_address.setOnFocusChangeListener(this);

//        this.tv_address.setOnFocusChangeListener(this);

//        try {
//            suburb_list = (ArrayList<String>) ObjectSerializer.deserialize(pref.getStringValues("Suburbs"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        adapter = new AutocompleteArrayAdapter(this.context, suburb_list);
//        this.tv_suburb.setAdapter(adapter);
//        this.tv_suburb.setThreshold(1);

        ClickListeners();


        return rootview;


    }


    private void ClickListeners() {
        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) addressActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(addressActivity.getCurrentFocus().getWindowToken(), 0);

                String suburb_text = tv_suburb.getText().toString().trim();
                String address_text = tv_address.getText().toString().trim();

                if (address_text.isEmpty() && suburb_text.isEmpty()) {
                    Toast toast = Toast.makeText(context,
                            "Please search for your address", Toast.LENGTH_LONG);
                    toast.show();
                }
//                else if (tv_address.getVisibility()!=View.VISIBLE) {
//
//                    if (suburb_text.equalsIgnoreCase("BERMAGUI")) {
//                        tv_address.setVisibility(View.VISIBLE);
//                    }
//                }
                else {
                    pref.setKeyValues("user_suburb", suburb_text);
                    pref.setKeyValues("user_address", address_text);

                    if (library.isConnectingToInternet()) {
                        getLocation.getLocationMethod(suburb_text, address_text, null);
                    }
                }
            }
        });

        btn_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addressActivity.bottomNavigation.setCurrentItem(2);

            }
        });
        checkRadio = "2";
//        radio_res.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkRadio = "2";
//                tv_suburb.setEnabled(true);
//                tv_address.setEnabled(true);
//                radio_guest.setChecked(false);
//            }
//        });
//
//        radio_guest.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkRadio = "1";
//                tv_suburb.setEnabled(false);
//                tv_address.setEnabled(false);
//                radio_res.setChecked(false);
//            }
//        });


    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
//        String suburb_txt = pref.getStringValues("Selected suburb");
//        if (this.tv_address.hasFocus()) {
//            if (this.library.isConnectingToInternet()) {
//                String suburb_editTxt = this.tv_suburb.getText().toString();
//                if (!suburb_editTxt.isEmpty()) {
//                    suburb_editTxt = toTitleCase(suburb_editTxt);
//                }
//                if (this.suburb_list.contains(suburb_editTxt)) {
//                    if (!this.tv_address.getText().toString().equals("0") || !suburb_editTxt.equals(suburb_txt)) {
//                        if (this.library.isConnectingToInternet()) {
//                            ArrayList<String> addressSuggestion = new ArrayList<String>();
//                            addressSuggestion = this.getAddressAuto.getAutoStreets(suburb_editTxt);
//                            adapter = new AutocompleteArrayAdapter(this.context, addressSuggestion);
//                            this.tv_address.setAdapter(adapter);
//                            this.tv_address.setThreshold(1);
//                        } else {
//                            Toast toast = Toast.makeText(this.context, "No internet connection!", Toast.LENGTH_SHORT);
//                            toast.show();
//                        }
//                        pref.setKeyValues("Selected suburb", suburb_editTxt);
////                        this.tv_address.setText("");
//                    }
//                } else {
//                    adapter = new AutocompleteArrayAdapter(this.context, null);
//                    this.tv_address.setAdapter(adapter);
//                    pref.setKeyValues("Selected suburb", "");
//                }
//            } else {
//                Toast toast = Toast.makeText(this.context, "No internet connection!", Toast.LENGTH_SHORT);
//                toast.show();
//            }
//        }

//        if (!this.tv_suburb.hasFocus()) {
//            Toast.makeText(context, "Focus change", Toast.LENGTH_SHORT).show();
//        }


    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }
}
