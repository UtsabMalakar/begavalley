package com.wasteinfo.begavalley.bega.HelperClasses;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.RetrofitModel.Sections;

import java.util.List;

public class MoreInformationArrayAdapter extends ArrayAdapter<List> {
	
	private Context context;
    private List<Object> list;
    private Library library;
    GPSTracker gps;
    double lng;
    double lat;
    Location locA;
    Location locB;
    

	public MoreInformationArrayAdapter(Context context, List list) {
		super(context, R.layout.list_item_more_information ,list);
		this.context = context;
        this.list = list;
        this.library=new Library(this.context);
        gps = new GPSTracker(context);
        this.lat = gps.getLatitude();
        this.lng = gps.getLongitude();
        this.locA = new Location("source");;
        this.locB = new Location("distance");
	}
	
	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView;
        
      //Check the class
        if(list.get(position).getClass().toString().equals(Sections.class.toString())){
        	rowView = inflater.inflate(R.layout.list_item_more_information, parent, false);
        	TextView value = (TextView) rowView.findViewById(R.id.list_item_more_value);
        	ImageView imgV= (ImageView) rowView.findViewById(R.id.list_item_more_image);
            value.setText(((Sections)list.get(position)).getTitle());
            imgV.setImageDrawable(this.context.getResources().getDrawable(R.drawable.arrow_right));
            
            //Treatment font
            value.setTypeface(this.library.getDefaultFont("regular"));
                        
        }else{
        	if(((Page)list.get(position)).getPageImage().isEmpty()){
                locA.setLatitude(lat);
                locA.setLongitude(lng);
        		rowView = inflater.inflate(R.layout.list_item_more_information, parent, false);
                TextView value = (TextView) rowView.findViewById(R.id.list_item_more_value);
                TextView distance = (TextView) rowView.findViewById(R.id.list_more_distance);
                value.setText(((Page)list.get(position)).getPageTitle());
                if (!((Page)list.get(position)).getLatitude().equals("")) {
                    locB.setLatitude(Double.parseDouble(((Page) list.get(position)).getLatitude()));
                    locB.setLongitude(Double.parseDouble(((Page) list.get(position)).getLongitude()));
                    String dist = String.format("%.2f", locA.distanceTo(locB) / 1000);
                    distance.setVisibility(View.VISIBLE);
                    distance.setText(dist + " km");
                }
                //Treatment font
                value.setTypeface(this.library.getDefaultFont("regular"));
                distance.setTypeface(this.library.getDefaultFont("regular"));
                
        	}else{
                Log.e("Else", "You are here");
        		String imagePath=((Page)list.get(position)).getPageImage();
        		rowView = inflater.inflate(R.layout.list_item_image_more, parent, false);
                ImageView left = (ImageView) rowView.findViewById(R.id.list_item_image_more_image_left);
                TextView text_1 = (TextView) rowView.findViewById(R.id.list_item_image_more_value_1);
                TextView text_2 = (TextView) rowView.findViewById(R.id.list_item_image_more_value_2);
                
              //Treatment text
                text_1.setText(((Page)list.get(position)).getPageTitle());
                text_2.setText(((Page)list.get(position)).getPageDescription());
                
                //Treatment font
                text_1.setTypeface(this.library.getDefaultFont("regular"));
                text_2.setTypeface(this.library.getDefaultFont("regular"));

                Picasso.with(this.context).load(imagePath).into(left);
                //DownloadImageTask.getInstance().loadBitmap(imagePath, left,this.context);
                
                
                //TCImageLoader imgLoader = new TCImageLoader(this.context);
                //imgLoader.display(imagePath, left);
                
                //ImageDownloader imgDownloader = new ImageDownloader(left,this.context);
                //imgDownloader.execute(imagePath);
                
                
        	}
        }
		
		return rowView;
	}

}
