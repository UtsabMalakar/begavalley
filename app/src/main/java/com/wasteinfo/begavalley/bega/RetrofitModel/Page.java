package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Test on 6/24/2016.
 */
public class Page {


    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("page_id")
    @Expose
    private long pageId;
    @SerializedName("page_title")
    @Expose
    private String pageTitle;
    @SerializedName("page_description")
    @Expose
    private String pageDescription;
    @SerializedName("page_image")
    @Expose
    private String pageImage;
    @SerializedName("section_id")
    @Expose
    private int sectionId;
    @SerializedName("type_of_calendar")
    @Expose
    private int typeOfCalendar;
    @SerializedName("section")
    @Expose
    private String section;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("order")
    @Expose
    private int order;
    @SerializedName("page_status")
    @Expose
    private int pageStatus;
    @SerializedName("allowed")
    @Expose
    private String allowed;
    @SerializedName("not_allowed")
    @Expose
    private String notAllowed;
    @SerializedName("bin_type_id")
    @Expose
    private int binTypeId;
    @SerializedName("bin_color_id")
    @Expose
    private int binColorId;
    @SerializedName("page_content")
    @Expose
    private String pageContent;

    /**
     *
     * @return
     * The pageId
     */
    public long getPageId() {
        return pageId;
    }

    /**
     *
     * @param pageId
     * The page_id
     */
    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    /**
     *
     * @return
     * The pageTitle
     */
    public String getPageTitle() {
        return pageTitle;
    }

    /**
     *
     * @param pageTitle
     * The page_title
     */
    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    /**
     *
     * @return
     * The pageDescription
     */
    public String getPageDescription() {
        return pageDescription;
    }

    /**
     *
     * @param pageDescription
     * The page_description
     */
    public void setPageDescription(String pageDescription) {
        this.pageDescription = pageDescription;
    }

    /**
     *
     * @return
     * The pageImage
     */
    public String getPageImage() {
        return pageImage;
    }

    /**
     *
     * @param pageImage
     * The page_image
     */
    public void setPageImage(String pageImage) {
        this.pageImage = pageImage;
    }

    /**
     *
     * @return
     * The sectionId
     */
    public int getSectionId() {
        return sectionId;
    }

    /**
     *
     * @param sectionId
     * The section_id
     */
    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    /**
     *
     * @return
     * The typeOfCalendar
     */
    public int getTypeOfCalendar() {
        return typeOfCalendar;
    }

    /**
     *
     * @param typeOfCalendar
     * The type_of_calendar
     */
    public void setTypeOfCalendar(int typeOfCalendar) {
        this.typeOfCalendar = typeOfCalendar;
    }

    /**
     *
     * @return
     * The section
     */
    public String getSection() {
        return section;
    }

    /**
     *
     * @param section
     * The section
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     *
     * @return
     * The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     * The order
     */
    public int getOrder() {
        return order;
    }

    /**
     *
     * @param order
     * The order
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     *
     * @return
     * The pageStatus
     */
    public int getPageStatus() {
        return pageStatus;
    }

    /**
     *
     * @param pageStatus
     * The page_status
     */
    public void setPageStatus(int pageStatus) {
        this.pageStatus = pageStatus;
    }

    /**
     *
     * @return
     * The allowed
     */
    public String getAllowed() {
        return allowed;
    }

    /**
     *
     * @param allowed
     * The allowed
     */
    public void setAllowed(String allowed) {
        this.allowed = allowed;
    }

    /**
     *
     * @return
     * The notAllowed
     */
    public String getNotAllowed() {
        return notAllowed;
    }

    /**
     *
     * @param notAllowed
     * The not_allowed
     */
    public void setNotAllowed(String notAllowed) {
        this.notAllowed = notAllowed;
    }

    /**
     *
     * @return
     * The binTypeId
     */
    public int getBinTypeId() {
        return binTypeId;
    }

    /**
     *
     * @param binTypeId
     * The bin_type_id
     */
    public void setBinTypeId(int binTypeId) {
        this.binTypeId = binTypeId;
    }

    /**
     *
     * @return
     * The binColorId
     */
    public int getBinColorId() {
        return binColorId;
    }

    /**
     *
     * @param binColorId
     * The bin_color_id
     */
    public void setBinColorId(int binColorId) {
        this.binColorId = binColorId;
    }

    public String getPageContent() {
        return pageContent;
    }

    public void setPageContent(String pageContent) {
        this.pageContent = pageContent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
