package com.wasteinfo.begavalley.bega.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitModel.Step;

import java.util.ArrayList;

public class MapActivity extends AppCompatActivity {

	private Context context;
	private GoogleMap map;
	SharedPreferences prefs;
	
	LatLng northeastPosition;
	LatLng southwestPosition;
	LatLng dest_position;
	LatLng position;
	LatLngBounds bounds;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_map);
//		Remove title bar
//		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//
//		//Remove notification bar
//		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
//
//
//		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		this.context=this;
		this.map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.direction_map)).getMap();
		this.map.getUiSettings().setZoomControlsEnabled(true);
//		this.map.getUiSettings().setCompassEnabled(true);

				   
		prefs = PreferenceManager.getDefaultSharedPreferences(this.context);
		String json=prefs.getString("Step_List","");
		String northeastLat = prefs.getString("NorthEastLat","");
		String northeastLng = prefs.getString("NorthEastLng","");
		String southwestLat = prefs.getString("SouthWestLat","");
		String southwestLng = prefs.getString("SouthWestLng","");

		try{
			northeastPosition = new LatLng(Double.parseDouble(northeastLat),Double.parseDouble(northeastLng));
		
			southwestPosition = new LatLng(Double.parseDouble(southwestLat),Double.parseDouble(southwestLng));
		
			bounds = new LatLngBounds(southwestPosition, northeastPosition);
		
		
		if(!json.isEmpty()){
			
			
			Gson gson = new Gson();
			
			java.lang.reflect.Type type = new TypeToken<ArrayList<Step>>(){}.getType();
			ArrayList<Step> stepList = gson.fromJson(json, type);
	        
			//Start location
			LatLng position = new LatLng(Double.parseDouble(stepList.get(0).getStartLat()), Double.parseDouble(stepList.get(0).getStartLng()));
			MarkerOptions marker = new MarkerOptions();
			marker.position(position);
			marker.draggable(false);
//			// marker.title(this.item.getMap_address());
//			this.map.moveCamera(CameraUpdateFactory.zoomTo(12));
//			this.map.moveCamera(CameraUpdateFactory.newLatLng(position));
			this.map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 400, 400, 10));
			this.map.addMarker(marker);	
	        
			int lengthOfStep=stepList.toArray().length;
			
			//End location
			LatLng dest_position = new LatLng(Double.parseDouble(stepList.get(lengthOfStep-1).getEndLat()), Double.parseDouble(stepList.get(lengthOfStep-1).getEndLng()));
			MarkerOptions marker_dest = new MarkerOptions();
			marker_dest.position(dest_position);
			marker_dest.draggable(false);
			this.map.addMarker(marker_dest);
			
	        for(int i=0; i < stepList.toArray().length;i++){
	        	
	        	Polyline line = map.addPolyline(new PolylineOptions()
	            .add(new LatLng(Double.parseDouble(stepList.get(i).getStartLat()),Double.parseDouble(stepList.get(i).getStartLng()) ), new LatLng(Double.parseDouble(stepList.get(i).getEndLat()),Double.parseDouble(stepList.get(i).getEndLng())))
	            .width(5)
	            .color(Color.GREEN));

	        	
	        }
			}
		}catch(Exception e){
			e.printStackTrace();
			Toast.makeText(this,"No route between source and destination!", Toast.LENGTH_SHORT);
		}
		
	}

//	@Override
//	protected void onStart() {
//		// TODO Auto-generated method stub
//		super.onStart();
//		MyAppTracker.getInstance().trackScreenView("MapActivity");
//	}


	@Override
	protected void onResume() {
		super.onResume();
//		MyAppTracker.getInstance().trackScreenView("MapActivity");

	}

//	@Override
//	protected void onStop() {
//		// TODO Auto-generated method stub
//		super.onStop();
//		GoogleAnalytics.getInstance(this).reportActivityStart(this);
//	}
	
}
