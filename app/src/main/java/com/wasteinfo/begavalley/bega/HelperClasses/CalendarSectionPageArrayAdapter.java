package com.wasteinfo.begavalley.bega.HelperClasses;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.RetrofitModel.Section;
import com.wasteinfo.begavalley.bega.RetrofitModel.Sections;

import java.util.List;

@SuppressLint("NewApi")
public class CalendarSectionPageArrayAdapter extends ArrayAdapter {

    private Context context;
    private List<Object> list;
    private Library library;


    public CalendarSectionPageArrayAdapter(Context context, List list) {
        super(context,  0 , list);
        this.context = context;
        this.list = list;
        this.library = new Library(this.context);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView;

      //Check the class

        if(list.get(position).getClass().toString().equals(Section.class.toString())){
        	rowView = inflater.inflate(R.layout.list_calendar_section_page, parent, false);
        	TextView value = (TextView) rowView.findViewById(R.id.list_item_calendar_value);
        	ImageView imgV= (ImageView) rowView.findViewById(R.id.list_item_calendar_image);
            value.setText(((Sections)list.get(position)).getTitle());
            imgV.setImageDrawable(this.context.getResources().getDrawable(R.drawable.arrow_right));

            //treatment font
            value.setTypeface(this.library.getDefaultFont("regular"));

        }else{

        	if(((Page)list.get(position)).getPageImage().isEmpty()){
        		rowView = inflater.inflate(R.layout.list_calendar_section_page, parent, false);
                TextView value = (TextView) rowView.findViewById(R.id.list_item_calendar_value);
                ImageView imgV= (ImageView) rowView.findViewById(R.id.list_item_calendar_image);
                value.setText(((Page)list.get(position)).getPageTitle());
                imgV.setImageDrawable(this.context.getResources().getDrawable(R.drawable.arrow_right));

              //treatment font
                value.setTypeface(this.library.getDefaultFont("regular"));

        	}else{
        		String imagePath=((Page)list.get(position)).getPageImage();
        		rowView = inflater.inflate(R.layout.list_item_image_calendar, parent, false);
                ImageView left = (ImageView) rowView.findViewById(R.id.list_item_image_calendar_image_left);
                TextView text_1 = (TextView) rowView.findViewById(R.id.list_item_image_calendar_value_1);

                TextView text_2 = (TextView) rowView.findViewById(R.id.list_item_image_calendar_value_2);
                ImageView imgV= (ImageView) rowView.findViewById(R.id.list_item_calendar_image);

                //Treatment text
                text_1.setText(((Page)list.get(position)).getPageTitle());
                text_2.setText(((Page)list.get(position)).getPageDescription());
                imgV.setImageDrawable(this.context.getResources().getDrawable(R.drawable.arrow_right));

                //Treatment font
                text_1.setTypeface(this.library.getDefaultFont("regular"));
                text_2.setTypeface(this.library.getDefaultFont("regular"));


                //DownloadImageTask.getInstance().loadBitmap(imagePath,left,this.context);
                Picasso.with(this.context).load(imagePath).into(left);


        	}
        }
//
		return rowView;
	}

//	@Override
//	public int getCount() {
//
//		return list.size()-1 ;
//	}

    }
//}

