package com.wasteinfo.begavalley.bega.WebServiceTasks;

import android.content.Context;

import com.wasteinfo.begavalley.bega.HelperClasses.ObjectSerializer;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitAPI.MoiraAPI;
import com.wasteinfo.begavalley.bega.RetrofitModel.Suburbs;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Test on 6/28/2016.
 */
public class GetSuburbs {

    private Context context;
    private String url;
    SharedPref sharedPref;

    public GetSuburbs(Context context){
        this.context = context;
        this.url = context.getResources().getString(R.string.url);
        this.sharedPref = new SharedPref(context);
    }

    public void getSuburb(){
        MoiraAPI moiraAPI = getMoiraAPI();


        Call<Suburbs> suburbs = moiraAPI.getSuburbs("");
        suburbs.enqueue(new Callback<Suburbs>() {
            @Override
            public void onResponse(Call<Suburbs> call, Response<Suburbs> response) {
                ArrayList<String> suburbs = new ArrayList<String>();
                for (int i = 0; i < response.body().getSuburbs().size(); i++) {
                    suburbs.add(response.body().getSuburbs().get(i));
                }

                try {
                    sharedPref.setKeyValues("Suburbs", ObjectSerializer.serialize(suburbs));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Suburbs> call, Throwable t) {
                    t.printStackTrace();
            }
        });
    }

    private MoiraAPI getMoiraAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(this.url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();
        return retrofit.create(MoiraAPI.class);
    }

    public ArrayList<String> getAutoStreets(String suburb){

        MoiraAPI api = getMoiraAPI();
        final ArrayList<String> addresslist = new ArrayList<String>();

        Call<Suburbs> address = api.getAutoStreet(suburb);
        address.enqueue(new Callback<Suburbs>() {
            @Override
            public void onResponse(Call<Suburbs> call, Response<Suburbs> response) {
                for (int i = 0; i < response.body().getStreets().size(); i++) {
                    addresslist.add(response.body().getStreets().get(i));

                }
            }

            @Override
            public void onFailure(Call<Suburbs> call, Throwable t) {

            }
        });
        return addresslist;

    }
}
