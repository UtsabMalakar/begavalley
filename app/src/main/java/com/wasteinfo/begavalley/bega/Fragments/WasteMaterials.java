package com.wasteinfo.begavalley.bega.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.HelperClasses.MoreInformationArrayAdapter;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.RetrofitModel.Sections;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.WebServiceTasks.GetSections;
import com.wasteinfo.begavalley.bega.db.PageDataSource;
import com.wasteinfo.begavalley.bega.db.SectionDataSource;

import java.util.ArrayList;
import java.util.List;


public class WasteMaterials extends Fragment implements AdapterView.OnItemClickListener{

    private ListView listView;
    private Library library;
    private Context context;
    private SharedPref pref;
    AddressActivity addressActivity;

    private SectionDataSource sectionDataSource;
    private PageDataSource pageDataSource;
    private GetSections sectionsPages;

    private List<Object> item_list;
    private List<Sections> sections;
    private List<Page> pages;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.addressActivity = (AddressActivity) context;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview= inflater.inflate(R.layout.fragment_waste_materials, container, false);
        this.context = container.getContext();
        addressActivity.tv_title.setText(this.context.getResources().getString(R.string.more_information));
        addressActivity.toggle.setDrawerIndicatorEnabled(true);
        this.listView = (ListView) rootview.findViewById(R.id.more_list);
        this.library = new Library(this.context);
        this.sectionDataSource = new SectionDataSource(this.context);
        this.pageDataSource = new PageDataSource(this.context);
        this.item_list = new ArrayList<Object>();
        this.sections = new ArrayList<Sections>();
        this.pages = new ArrayList<Page>();

        this.pref = new SharedPref(this.context);
        this.sectionsPages = new GetSections(this.context);
        long section_id1 = getArguments().getLong("section_id");
        int section_id = (int) section_id1;
        if (section_id==0 ){
            section_id =1;
        }

        int flag = pref.getIntValues("dbFlag");

        if (flag == 0) {
            if (this.library.isConnectingToInternet()) {
                sectionsPages.getSectionPage(1, 0, this.item_list);

            }
            else {
                Toast toast = Toast.makeText(this.context, "No internet connection!", Toast.LENGTH_SHORT);
                toast.show();
            }
        }



        else{
            Sections section = new Sections();
            this.sectionDataSource.open();
            section = this.sectionDataSource.getSection(section_id);
            this.sections = this.sectionDataSource.getSections(section_id);
            this.sectionDataSource.close();
            if (sections!=null){
                for (int i = 0; i < sections.toArray().length; i++) {
                    this.item_list.add(this.sections.get(i));

                }
            }

            this.pageDataSource.open();
            this.pages = this.pageDataSource.getPages(section_id);
            if (this.pages != null) {
                for (int i = 0; i < this.pages.toArray().length; i++) {
                    this.item_list.add(this.pages.get(i));
                }
            }
            pageDataSource.close();

            if (this.item_list.toArray().length > 0) {
                this.listView.invalidateViews();
                this.listView.setAdapter(new MoreInformationArrayAdapter(this.context, this.item_list));

            }

        }

        this.listView.setOnItemClickListener(this);







        return rootview;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {

        if (this.item_list.get(i).getClass().toString().equals(Page.class.toString())) {
            WasteMaterialItem materialItem = new WasteMaterialItem();
            Bundle args = new Bundle();
            args.putInt("section_id",1);
            List<Object> list = new ArrayList<Object>();
            list.add(item_list.get(i));
            long page_id = ((Page) this.item_list.get(i)).getId();
            args.putLong("page_id",page_id);
            materialItem.setArguments(args);
            getFragmentManager().beginTransaction().replace(R.id.contentID,materialItem).commit();


        } else {
            WasteMaterialItem materialItem1 = new WasteMaterialItem();
            Bundle args = new Bundle();
            args.putInt("bin_type_id",3);
            args.putLong("section_id",((Sections) this.item_list.get(i)).getSectionId());
            materialItem1.setArguments(args);
            getFragmentManager().beginTransaction().replace(R.id.contentID,materialItem1).commit();
        }

    }
}
