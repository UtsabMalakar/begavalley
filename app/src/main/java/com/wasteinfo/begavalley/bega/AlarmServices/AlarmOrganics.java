package com.wasteinfo.begavalley.bega.AlarmServices;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Activities.MainActivity;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;

/**
 * Created by Test on 10/25/2016.
 */

public class AlarmOrganics extends BroadcastReceiver {

    private static final String LOGTAG = "WasteInfoVal";
    private Library library;
    private Context context;
    Notification notify;
    SharedPref pref;
    private boolean check_next;

    @SuppressWarnings("deprecation")
    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;
        pref = new SharedPref(this.context);
        Intent resultIntent = new Intent(this.context, MainActivity.class);
        CharSequence title = "Bega Valley";
        String details = "Bin collection tomorrow.";
        long time = System.currentTimeMillis();
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;

//        notify = new Notification(R.drawable.ic_notification_icon,details,time);

        NotificationManager nm = (NotificationManager) this.context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(this.context);
        // What time to show on the notification
//		Notification notify = new Notification(R.drawable.ic_launch,"Time to put your rubbish bin out.",time);

//        notify.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
//        notify.defaults = Notification.DEFAULT_SOUND;

        // The PendingIntent to launch our activity if the user selects this notification
//        PendingIntent contentIntent = PendingIntent.getActivity(this.context, 0, new Intent(this.context, MainActivity.class), 0);
        resultIntent.putExtra("msg",details);
        PendingIntent contentIntent= PendingIntent.getActivity(this.context, 1,
                resultIntent, PendingIntent.FLAG_ONE_SHOT);
        pref.setKeyValues("notification_message",details);

        // Set the info for the views that show in the notification panel.

        builder.setContentTitle(title);
        builder.setContentText(details);
        builder.setSmallIcon(R.drawable.ic_notification_icon);
        builder.setDefaults(defaults);
        builder.setAutoCancel(true);
//        builder.setSound(Uri.parse(String.valueOf(Notification.DEFAULT_SOUND)));
        builder.setContentIntent(contentIntent);
        builder.setOngoing(true);
        builder.setWhen(time);
        builder.build();

//		notify.setLatestEventInfo(this.context,title,details,contentIntent);

        // Clear the notification when it is pressed
//        notify.flags |= Notification.FLAG_AUTO_CANCEL;

        // Send the notification to the system.
        notify = builder.getNotification();
        nm.notify(0, notify);

        this.check_next = false;


    }

    public boolean getcheckNext() {
        return check_next;

    }
}
