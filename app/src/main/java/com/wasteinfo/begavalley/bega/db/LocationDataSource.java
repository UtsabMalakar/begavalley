package com.wasteinfo.begavalley.bega.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.wasteinfo.begavalley.bega.RetrofitModel.LocationResult;


public class LocationDataSource {
	
	private static final String LOGTAG = "WasteInfoVal";
	
	SQLiteOpenHelper dbhelper;
	SQLiteDatabase database;
	
	public LocationDataSource(Context context) {
		dbhelper = new WasteInfoDBOpenHelper(context);
		
	}

	public void open(){
		Log.d(LOGTAG,"Database opened!");
		database = dbhelper.getWritableDatabase();
	}
	
	public void close(){
		Log.d(LOGTAG,"Database closed!");
		dbhelper.close();
	}
	
	public LocationResult insertLocation(LocationResult location){
		ContentValues values = new ContentValues();
		values.put(WasteInfoDBOpenHelper.COLUMN_UNIT_NUMBER, location.getUnitNumber());
		values.put(WasteInfoDBOpenHelper.COLUMN_HOUSE_NUMBER, location.getHouseNumber());
		values.put(WasteInfoDBOpenHelper.COLUMN_STREET_NAME, location.getStreet());
		values.put(WasteInfoDBOpenHelper.COLUMN_STREET_TYPE, location.getStreet_type());
		values.put(WasteInfoDBOpenHelper.COLUMN_SUBURB, location.getSuburb());
		values.put(WasteInfoDBOpenHelper.COLUMN_POSTCODE, location.getPostcode());
		values.put(WasteInfoDBOpenHelper.COLUMN_COLLECTION_DAY, location.getCollectionDay());
		values.put(WasteInfoDBOpenHelper.COLUMN_BIN_TYPE, location.getBinType());
		values.put(WasteInfoDBOpenHelper.COLUMN_DATE_TIME, location.getDateTime());
		values.put(WasteInfoDBOpenHelper.COLUMN_BIN_COLORS, location.getBinColors());
		values.put(WasteInfoDBOpenHelper.COLUMN_COLLECTION_FREQUENCIES, location.getCollectionFrequencies());
		values.put(WasteInfoDBOpenHelper.COLUMN_AREA, location.getArea());
		values.put(WasteInfoDBOpenHelper.COLUMN_ITEM_GARBAGE_DATE, location.getItemGarbage());
		values.put(WasteInfoDBOpenHelper.COLUMN_ITEM_ORGANIC_DATE, location.getItemOrganics());
		values.put(WasteInfoDBOpenHelper.COLUMN_ITEM_RECYCLING_DATE, location.getItemRecycling());
		
		
		long item_id = database.insert(WasteInfoDBOpenHelper.TABLE_LOCATION, null, values); //P.k -> item_id
		location.setItemId(item_id);
		return location;
		
	}
	
	public String getGarbageDateByItemId(long item_id){
		String garbageDate = null;
		Cursor c=database.rawQuery("Select "+ WasteInfoDBOpenHelper.COLUMN_ITEM_GARBAGE_DATE +" from "+ WasteInfoDBOpenHelper.TABLE_LOCATION+" Where "+ WasteInfoDBOpenHelper.COLUMN_ITEM_ID +"="+item_id ,null);
		while(c.moveToNext()){
		garbageDate=c.getString(c.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ITEM_GARBAGE_DATE));
		}
		c.close();
		Log.d(LOGTAG,"garbage date==="+garbageDate);
		return garbageDate;
	}
	
	public String getRecyclingDateByItemId(long item_id){
		String recyclingDate=null;
		Cursor c=database.rawQuery("Select "+ WasteInfoDBOpenHelper.COLUMN_ITEM_RECYCLING_DATE+" from "+ WasteInfoDBOpenHelper.TABLE_LOCATION+" Where "+ WasteInfoDBOpenHelper.COLUMN_ITEM_ID +"="+item_id ,null);
		while(c.moveToNext()){
		recyclingDate=c.getString(c.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ITEM_RECYCLING_DATE));
		}
		c.close();
		Log.d(LOGTAG,"recycking date==="+recyclingDate);
		return recyclingDate;
	}
	
	public String getOrganicDateByItemId(long item_id){
		String organicDate=null;
		Cursor c=database.rawQuery("Select "+ WasteInfoDBOpenHelper.COLUMN_ITEM_ORGANIC_DATE +" from "+ WasteInfoDBOpenHelper.TABLE_LOCATION+" Where "+ WasteInfoDBOpenHelper.COLUMN_ITEM_ID+"="+item_id ,null);
		while(c.moveToNext()){
		organicDate=c.getString(c.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ITEM_ORGANIC_DATE));
		}
		c.close();
		Log.d(LOGTAG,"organic date==="+organicDate);
		return organicDate;
	}
	
	
	
}
