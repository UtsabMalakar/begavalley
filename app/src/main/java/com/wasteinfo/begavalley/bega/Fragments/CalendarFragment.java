package com.wasteinfo.begavalley.bega.Fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.HelperClasses.CalendarDate;
import com.wasteinfo.begavalley.bega.HelperClasses.CalendarItemArrayAdapter;
import com.wasteinfo.begavalley.bega.HelperClasses.CalendarLabelItemArrayAdapter;
import com.wasteinfo.begavalley.bega.HelperClasses.CalendarMonth;
import com.wasteinfo.begavalley.bega.HelperClasses.CalendarSectionPageArrayAdapter;
import com.wasteinfo.begavalley.bega.HelperClasses.ObjectSerializer;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.RetrofitModel.Section;
import com.wasteinfo.begavalley.bega.RetrofitModel.Sections;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.WebServiceTasks.GetSections;
import com.wasteinfo.begavalley.bega.db.LocationDataSource;
import com.wasteinfo.begavalley.bega.db.PageDataSource;
import com.wasteinfo.begavalley.bega.db.SectionDataSource;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */

public class CalendarFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    AddressActivity addressActivity;
    private static final String LOGTAG = "WasteInfoVal";
    private Context context;
    private CalendarMonth month;

    private Library library;
    private LocationDataSource datasource;
    private ArrayList<Calendar> date_list;
    private int month_id;
    private int year_id;
    private int day_id;
    private long item_id;
    private int bin_type_id;
    private String bin_colors;
    private String suburb_text;
    private String day;
    private String week_day;
    private String street_name;
    private String push_notification_msg;
    private ArrayList<Calendar> attachmentDates;
    String notification_message;

    private GetSections sectionsPages;

    private ArrayList<CalendarDate> list_dates;

    private int section_id;
    private Section section;
    private Page page;
    private List<Page> pages;
    private List<Sections> sections;
    private List<Object> item_list;

    private GridView calendar;
    private GridView label_days;
    private TextView label_title;
    private TextView label_month;
    private TextView next_pick_up_date;
    private TextView tv_helper;
    private Button btn_reminder;
    private Button btn_garbage;
    private Button btn_organics;
    private Button btn_recycling;
    private ListView list;
    private int current_item;
    private CalendarFragment calendarFragment;
    private BinItemFragment binItemFragment;

    private ImageButton btn_preview;
    private ImageButton btn_next;

    private SectionDataSource sectiondatasource;
    private PageDataSource pagedatasource;
    private String set_reminder_flag;
    private String flag_check_alarm;
    String output;
    private View reminder_frame;

    private SharedPref pref;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        addressActivity = (AddressActivity) context;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_calendar, container, false);
        this.library = new Library(this.context);


//        this.label_title = (TextView) rootview.findViewById(R.id.calendar_label_title);
        addressActivity.toggle.setDrawerIndicatorEnabled(true);
        addressActivity.closeDrawer();
//        label_title.setText("Test");
        this.list = (ListView) rootview.findViewById(R.id.calendar_list);

        this.btn_preview = (ImageButton) rootview.findViewById(R.id.calendar_btn_preview);
        this.btn_next = (ImageButton) rootview.findViewById(R.id.calendar_btn_next);
        this.calendar = (GridView) rootview.findViewById(R.id.calendar_day);
        this.label_month = (TextView) rootview.findViewById(R.id.calendar_label_month);
//        this.tv_helper = (TextView) findViewById(R.id.tv_helper);
        this.label_days = (GridView) rootview.findViewById(R.id.calendar_label_day);
        this.calendarFragment = new CalendarFragment();
        this.binItemFragment = new BinItemFragment();

        sectiondatasource = new SectionDataSource(this.context);
        pagedatasource = new PageDataSource(this.context);
        this.item_list = new ArrayList<Object>();

        pref = new SharedPref(this.context);

        this.item_id = pref.getIntValues("item_id");
        this.bin_type_id = 3;
        this.bin_colors = pref.getStringValues("bin_colors");
        this.suburb_text = pref.getStringValues("suburb");
        this.day = pref.getStringValues("day");

        this.street_name = pref.getStringValues("street_name");
        if (this.street_name.isEmpty()) {
            this.street_name = pref.getStringValues("suburb");
        }
        //this.set_reminder_flag = pref.getStringValues()("set_reminder_flag","0");
        this.flag_check_alarm = pref.getStringValues("flag_check_alarm");

        this.btn_next.setOnClickListener(this);
        this.btn_preview.setOnClickListener(this);
        int height = addressActivity.bottomNavigation.getHeight();


//        LayoutInflater headerLayout = addressActivity.getLayoutInflater();
        View listHeaderView = addressActivity.getLayoutInflater().inflate(R.layout.header_layout, null);
//        this.tv_helper = (TextView) listHeaderView.findViewById(R.id.tv_helper);
//        this.tv_helper.setTypeface(this.library.getDefaultFont("regular"));
//
//
        this.list.addHeaderView(listHeaderView);
//
//
        this.btn_garbage = (Button) listHeaderView.findViewById(R.id.calendar_btn_garbage);
        this.btn_organics = (Button) listHeaderView.findViewById(R.id.calendar_btn_organics);
        this.btn_recycling = (Button) listHeaderView.findViewById(R.id.calendar_btn_recycling);
        this.btn_reminder = (Button) listHeaderView.findViewById(R.id.calendar_btn_set_reminder);

        this.btn_garbage.setOnClickListener(this);
        this.btn_organics.setOnClickListener(this);
        this.btn_recycling.setOnClickListener(this);
        this.btn_reminder.setOnClickListener(this);
        this.list.setOnItemClickListener(this);


        if (null != this.day && this.day.length() != 0) {

            this.week_day = this.day.substring(0, 1).toUpperCase() + this.day.substring(1).toLowerCase();

            //Log.d(LOGTAG,"Day==="+week_day);

        }

        if (null != this.street_name && this.street_name.length() != 0) {

            output = this.street_name.substring(0, 1).toUpperCase() + this.street_name.substring(1).toLowerCase();
            output = toTitleCase(output);
            Log.d(LOGTAG, "StreetName===" + output);
            addressActivity.tv_title.setText(output);

//            label_title.setText(output);
        }

        NotificationReciever();


        int flag = pref.getIntValues("dbFlag");


        if (flag == 0) {
            if (this.library.isConnectingToInternet()) {
                //retrieve the sections and pages from the webservice
                Log.d(LOGTAG, "flag=0");


//                AsyncTaskPageInfo asyncTaskRunner2 = new AsyncTaskPageInfo(this,this.tv_helper);
//                asyncTaskRunner2.execute(64);
//                asyncTaskRunner2.delegate = this;
                sectionsPages.getSectionPage(1, 0, this.item_list);


            }
            if (this.library.isConnectingToInternet()) {
                //retrieve the page title for Info Grey bar from the webservice
                Log.d(LOGTAG, "flag=0");


            } else {
                Toast toast = Toast.makeText(this.context, "No internet connection!", Toast.LENGTH_SHORT);
                toast.show();
            }
        } else {
            try {
                Sections section = new Sections();
                this.sectiondatasource.open();
                section = this.sectiondatasource.getSection(1);
                this.sections = this.sectiondatasource.getSections(1);
                this.sectiondatasource.close();

                pagedatasource.open();
                this.page = this.pagedatasource.getPage(64);
                this.pages = pagedatasource.getPagesForCalendarPage(this.section_id, 3);
//                this.pages = pagedatasource.getPagesBySection(4);
//                this.tv_helper.setText("   "+this.page.getPageTitle());

//			this.pages=null;

//                if (this.sections != null) {
//                    for (int i = 0; i < this.sections.toArray().length; i++) {
//                        this.item_list.add(this.sections.get(i));
//                    }
//
//                }

                if (this.pages != null) {
                    for (int i = 0; i < this.pages.toArray().length; i++) {
                        this.item_list.add(this.pages.get(i));
                    }
                }


                Collections.sort(this.item_list, new Comparator<Object>() {
                    public int compare(Object obj1, Object obj2) {
                        String str1 = "";
                        String str2 = "";

                        if (obj1.getClass().toString().equals(Section.class.toString())) {
                            str1 = ((Sections) obj1).getTitle();
                        } else if (obj1.getClass().toString().equals(Page.class.toString())) {
                            str1 = ((Page) obj1).getPageTitle();
                        }

                        if (obj2.getClass().toString().equals(Section.class.toString())) {
                            str2 = ((Sections) obj2).getTitle();
                        } else if (obj2.getClass().toString().equals(Page.class.toString())) {
                            str2 = ((Page) obj2).getPageTitle();
                        }


                        return str1.compareToIgnoreCase(str2);
                    }
                });

                pagedatasource.close();

                if (this.item_list.toArray().length > 0) {
                    this.list.invalidateViews();
                    this.list.setAdapter(new CalendarSectionPageArrayAdapter(this.context, this.item_list));
                }
                getPagesSections(this.item_list);

                this.list.invalidateViews();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Calendar rightNow = Calendar.getInstance();


        this.year_id = getArguments().getInt("year_id", rightNow.get(Calendar.YEAR));
        this.month_id = getArguments().getInt("month_id", rightNow.get(Calendar.MONTH));
        this.day_id = getArguments().getInt("day_id", rightNow.get(Calendar.DATE));
        this.month = new CalendarMonth(month_id, year_id, this.context);
        this.current_item = 0;

        Log.d(LOGTAG, "CurrentDate=" + this.day_id);

        datasource = new LocationDataSource(this.context);
        datasource.open();
        String extractedOrganicDate = datasource.getOrganicDateByItemId(item_id);

        addressActivity.ButtonHide();

        if (extractedOrganicDate == null || extractedOrganicDate.isEmpty()) {
            this.btn_organics.setVisibility(View.GONE);
//            this.reminder_frame.getLayoutParams().width = 1;
        }

        String extractedRecyclingDate = datasource.getRecyclingDateByItemId(item_id);
        if (extractedRecyclingDate == null || extractedRecyclingDate.isEmpty()) {
            this.btn_recycling.setVisibility(View.GONE);

        }

        String extractedGarbageDate = datasource.getGarbageDateByItemId(item_id);
        if (extractedGarbageDate == null || extractedGarbageDate.isEmpty()) {
            this.btn_garbage.setVisibility(View.GONE);
        }

        this.list_dates = this.month.getMonth_date();


        boolean checkFirstDate = false;

        if (this.month != null)
            for (int i = 0; i < this.list_dates.toArray().length; i++) {
                if (list_dates.get(i) != null) {
                    if (this.list_dates.get(i).getDate().get(Calendar.DATE) == 1) {
                        checkFirstDate = true;
                    }
                    if (checkFirstDate) {
                        if (this.list_dates.get(i).getDate().get(Calendar.DATE) == this.day_id) {
                            this.current_item = i;
                            break;
                        }
                    }
                }
            }

        Log.d(LOGTAG, "Current Item==" + this.current_item);

        Date date = new Date(this.year_id - 1900, this.month_id, 1);

        this.label_month.setText(new SimpleDateFormat("MMMM yyyy").format(date));

        if (this.list_dates != null) {
            this.calendar.setAdapter(new CalendarItemArrayAdapter(this.context, this.list_dates, this.month_id, this.day_id));
        }

        List<String> days = new ArrayList<String>();
        days.add("Sun");
        days.add("Mon");
        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");

        checkFirstDate = false;
        boolean checkCurrentDate = false;
        this.attachmentDates = new ArrayList<Calendar>();
        for (int i = 0; i < this.list_dates.toArray().length; i++) {
            if (this.list_dates.get(i) != null) {

                if (this.list_dates.get(i).getDate().get(Calendar.DATE) == 1) {
                    checkFirstDate = true;
                }

                if (checkFirstDate) {
                    if (this.list_dates.get(i).getDate().get(Calendar.DATE) == this.day_id) {
                        checkCurrentDate = true;
                    }

                    if (checkCurrentDate) {

                        if (!this.list_dates.get(i).getAttachment().isEmpty()) {
                            this.attachmentDates.add(this.list_dates.get(i).getDate());
                        }
                    }
                }
            }
        }


        if (this.month != null) {

        }
        try {
            pref.setKeyValues("Calendar_Date_List", ObjectSerializer.serialize(this.attachmentDates));
        } catch (IOException e) {
            e.printStackTrace();
        }


        //retrieve the attachment dates from the shared preferences
        try {

            date_list = (ArrayList<Calendar>) ObjectSerializer.deserialize(pref.getStringValues("Calendar_Date_List"));


        } catch (IOException e) {
            e.printStackTrace();
        }

        this.label_days.setAdapter(new CalendarLabelItemArrayAdapter(this.context, days, this.week_day, this.year_id, this.month_id));

        return rootview;
    }

    private void NotificationReciever() {
        this.pref = new SharedPref(this.context);
        this.sectionsPages = new GetSections(this.context);
        section_id = getArguments().getInt("section_id");
        notification_message = pref.getStringValues("notification_message");

        try {
            if (!this.notification_message.isEmpty()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
                builder.setMessage(this.notification_message);
                builder.setTitle("Bega Valley Waste App");
                builder.setCancelable(false);

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pref.setKeyValues("notification_message", null);
                        dialog.cancel();
                    }
                });


                AlertDialog alert = builder.create();
                alert.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String toTitleCase(String givenString) {
        String[] arr = givenString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == this.btn_preview.getId()) {
            Log.d("testVal", "Preview Button Clicked!");
            final ProgressDialog pd = ProgressDialog.show(this.context, "", "Loading...", true);
            Bundle args = new Bundle();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    pd.dismiss();
                }
            }, 1200);  // 1200 milliseconds
//            this.myself.putExtra("day_id", -1);
            if (this.month_id - 1 >= 0) {
                args.putInt("month_id", this.month_id - 1);
                args.putInt("year_id", this.year_id);
            } else {
                this.year_id = this.year_id - 1;
                args.putInt("year_id", this.year_id);
                args.putInt("month_id", 11);
            }
            args.putInt("section_id", 3);
            calendarFragment.setArguments(args);
            addressActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentID, calendarFragment).commit();
            // pd.dismiss();

        }

        if (v.getId() == this.btn_next.getId()) {
//			try{
//				GoogleAnalyticsHelper.trackButtonClick(this,"Calendar Page(Android)","EVENTS","Button Press","Report Problem Button");
//			}catch(Exception e){
//				e.printStackTrace();
//			}
            final ProgressDialog pd = ProgressDialog.show(this.context, "", "Loading...", true);
            Handler handler = new Handler();
            Bundle args = new Bundle();
            handler.postDelayed(new Runnable() {
                public void run() {
                    pd.dismiss();
                }
            }, 1200);  // 2000 milliseconds
//            this.myself.putExtra("day_id", -1);
            if (this.month_id + 1 < 12) {
                args.putInt("month_id", this.month_id + 1);
                args.putInt("year_id", this.year_id);
            } else {
                this.year_id = this.year_id + 1;
                args.putInt("year_id", this.year_id);
                args.putInt("month_id", 0);
            }
            args.putInt("section_id", 3);
            calendarFragment.setArguments(args);
            addressActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentID, calendarFragment).commit();
            // pd.dismiss();
        }


        if (v.getId() == this.btn_garbage.getId()) {
            Bundle args = new Bundle();
            args.putInt("section_id", 3);
            args.putInt("bin_color", 1);
            binItemFragment.setArguments(args);
            addressActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.contentID, binItemFragment).commit();


        }

        if (v.getId() == this.btn_recycling.getId()) {
            Bundle args = new Bundle();
            args.putInt("section_id", 3);
            args.putInt("bin_color", 3);
            binItemFragment.setArguments(args);
            addressActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.contentID, binItemFragment).commit();

        }

        if (v.getId() == this.btn_organics.getId()) {
            Bundle args = new Bundle();
            args.putInt("section_id", 3);
            args.putInt("bin_color", 2);
            binItemFragment.setArguments(args);
            addressActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.contentID, binItemFragment).commit();

        }

        if (v.getId() == this.btn_reminder.getId()) {
            addressActivity.drawer.openDrawer(Gravity.LEFT);
        }

    }

    public void getPagesSections(List<Object> item_list) {
        this.item_list = item_list;
        if (this.item_list.toArray().length > 0) {
            this.list.setAdapter(new CalendarSectionPageArrayAdapter(addressActivity, this.item_list));
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == this.list.getId()) {
            if (this.item_list.get(position - 1).getClass().toString().equalsIgnoreCase(Page.class.toString())) {
                long page_id;
                if (((Page) this.item_list.get(position - 1)).getPageId() != 63) {
                    WasteMaterialItem materialItem = new WasteMaterialItem();
                    Bundle args = new Bundle();
                    args.putInt("section_id", 3);
                    List<Object> list = new ArrayList<Object>();
                    list.add(item_list.get(position - 1));
                    page_id = ((Page) this.item_list.get(position - 1)).getId();
                    args.putLong("page_id", page_id);
                    materialItem.setArguments(args);
                    getFragmentManager().beginTransaction().replace(R.id.contentID, materialItem).commit();
                } else {
                    WasteMaterials materialItem1 = new WasteMaterials();
                    Bundle args = new Bundle();
                    args.putInt("bin_type_id", 3);
                    args.putLong("section_id", 4);
                    materialItem1.setArguments(args);
                    getFragmentManager().beginTransaction().replace(R.id.contentID, materialItem1).commit();
                }
            } else {
                Toast.makeText(this.context, "This is a section", Toast.LENGTH_SHORT).show();
            }
        }
    }

//    int flag = pref.getIntValues("flag");

}
