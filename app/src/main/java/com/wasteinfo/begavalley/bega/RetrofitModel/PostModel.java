package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Test on 7/18/2016.
 */
public class PostModel {

    @SerializedName("code")
    @Expose
    public String success;
}
