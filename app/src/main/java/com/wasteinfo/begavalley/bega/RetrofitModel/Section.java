package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Test on 6/24/2016.
 */
public class Section {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("pages")
    @Expose
    private List<Page> pages = new ArrayList<Page>();
    @SerializedName("sections")
    @Expose
    private List<Sections> sections = new ArrayList<Sections>();
    @SerializedName("page_detail")
    @Expose
    private List<Page> pageDetail = new ArrayList<Page>();

    /**
     *
     * @return
     * The code
     */
    public String getCode() {
        return code;
    }

    /**
     *
     * @param code
     * The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     *
     * @return
     * The msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param msg
     * The msg
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     *
     * @return
     * The pages
     */
    public List<Page> getPages() {
        return pages;
    }

    /**
     *
     * @param pages
     * The pages
     */
    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

    /**
     *
     * @return
     * The sections
     */
    public List<Sections> getSections() {
        return sections;
    }

    /**
     *
     * @param sections
     * The sections
     */
    public void setSections(List<Sections> sections) {
        this.sections = sections;
    }

    public List<Page> getPageDetail() {
        return pageDetail;
    }

    public void setPageDetail(List<Page> pageDetail) {
        this.pageDetail = pageDetail;
    }
}

