package com.wasteinfo.begavalley.bega.db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.wasteinfo.begavalley.bega.RetrofitModel.Page;

import java.util.ArrayList;
import java.util.List;

public class PageDataSource {
	private static final String LOGTAG = "WasteInfoVal";
	SQLiteOpenHelper dbhelper;
	SQLiteDatabase database;
	
	public PageDataSource(Context context) {
		dbhelper = new WasteInfoDBOpenHelper(context);
		
	}
	
	public void open(){
		Log.d(LOGTAG,"Database opened!");
		database = dbhelper.getWritableDatabase();
	}
	
	public void close(){
		Log.d(LOGTAG,"Database closed!");
		dbhelper.close();
	}
	
	public void insertOrUpdatePage(Page page){
		long id=page.getId();
		Cursor c=database.rawQuery("SELECT * FROM "+ WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PAGE+" WHERE "+ WasteInfoDBOpenHelper.COLUMN_ID+"="+id, null);
		int count=c.getCount();
		if(c.getCount() == 0){
			//if the row is not present,then insert it
			ContentValues values = new ContentValues();
			values.put(WasteInfoDBOpenHelper.COLUMN_ID, id);
			values.put(WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE, page.getPageTitle());
			values.put(WasteInfoDBOpenHelper.COLUMN_PAGE_DESCRIPTION, page.getPageDescription());
			values.put(WasteInfoDBOpenHelper.COLUMN_PAGE_CONTENT,page.getPageContent());
			values.put(WasteInfoDBOpenHelper.COLUMN_PAGE_IMAGE,page.getPageImage());
			values.put(WasteInfoDBOpenHelper.COLUMN_SECTION_ID,page.getSectionId());
			values.put(WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR,page.getTypeOfCalendar());
			values.put(WasteInfoDBOpenHelper.COLUMN_LATITUDE,page.getLatitude());
			values.put(WasteInfoDBOpenHelper.COLUMN_LONGITUDE,page.getLongitude());
			values.put(WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER,page.getOrder());
			values.put(WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS,page.getPageStatus());
			values.put(WasteInfoDBOpenHelper.COLUMN_ALLOWED,page.getAllowed());
			values.put(WasteInfoDBOpenHelper.COLUMN_NOT_ALLOWED,page.getNotAllowed());
			values.put(WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID,page.getBinTypeId());
			values.put(WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID,page.getBinColorId());
			
			database.insert(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PAGE, null, values);
		
		
		}else{
			//if the row is already present,then update that row
			ContentValues updatedValues =new ContentValues();
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE,page.getPageTitle());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_PAGE_DESCRIPTION, page.getPageDescription());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_PAGE_CONTENT,page.getPageContent());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_PAGE_IMAGE,page.getPageImage());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_SECTION_ID,page.getSectionId());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR,page.getTypeOfCalendar());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_LATITUDE,page.getLatitude());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_LONGITUDE,page.getLongitude());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER,page.getOrder());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS,page.getPageStatus());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_ALLOWED,page.getAllowed());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_NOT_ALLOWED,page.getNotAllowed());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID,page.getBinTypeId());
			updatedValues.put(WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID,page.getBinColorId());
			
			database.update(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PAGE,updatedValues, WasteInfoDBOpenHelper.COLUMN_ID+"="+ id ,null );
		}	
			
		
		
	}
	
	public Page getPage(int page_id){
		Cursor c_page = this.database.query(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PAGE, new String[] {
				WasteInfoDBOpenHelper.COLUMN_ID,
				WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_DESCRIPTION,
				WasteInfoDBOpenHelper.COLUMN_PAGE_CONTENT,
				WasteInfoDBOpenHelper.COLUMN_PAGE_IMAGE,
				WasteInfoDBOpenHelper.COLUMN_SECTION_ID,
				WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR,
				WasteInfoDBOpenHelper.COLUMN_LATITUDE,
				WasteInfoDBOpenHelper.COLUMN_LONGITUDE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER,
				WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS,
				WasteInfoDBOpenHelper.COLUMN_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_NOT_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID,
				WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID
				}
				, WasteInfoDBOpenHelper.COLUMN_ID+"="+page_id ,null,null,null,null);
		if(c_page.getCount() != 0){
			c_page.moveToNext();
			Page page=new Page();
			page.setId(c_page.getLong(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ID)));
			page.setPageTitle(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE)));
			page.setPageDescription(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_DESCRIPTION)));
			page.setPageContent(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_CONTENT)));
			page.setPageImage(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_IMAGE)));
			page.setSectionId(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_SECTION_ID)));
			page.setTypeOfCalendar(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR)));
			page.setLatitude(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_LATITUDE)));
			page.setLongitude(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_LONGITUDE)));
			page.setOrder(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER)));
			page.setPageStatus(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS)));
			page.setAllowed(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ALLOWED)));
			page.setNotAllowed(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_NOT_ALLOWED)));
			page.setBinTypeId(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID)));
			page.setBinColorId(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID)));
			c_page.close();
			return page;
		}
		c_page.close();
		return null;
		
	}
	
	public ArrayList<Page> getPagesBySection(int section_id){
		Cursor c_pages=this.database.query(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PAGE, new String[]{
				WasteInfoDBOpenHelper.COLUMN_ID,
				WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_DESCRIPTION,
				WasteInfoDBOpenHelper.COLUMN_PAGE_CONTENT,
				WasteInfoDBOpenHelper.COLUMN_PAGE_IMAGE,
				WasteInfoDBOpenHelper.COLUMN_SECTION_ID,
				WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR,
				WasteInfoDBOpenHelper.COLUMN_LATITUDE,
				WasteInfoDBOpenHelper.COLUMN_LONGITUDE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER,
				WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS,
				WasteInfoDBOpenHelper.COLUMN_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_NOT_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID,
				WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID
				}, WasteInfoDBOpenHelper.COLUMN_SECTION_ID+"="+section_id +" AND "+ WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS+"="+1, null, null, null, WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER+ " ASC");
		
		if(c_pages.getCount() != 0){
			ArrayList<Page> pages = new ArrayList<Page>();
			while(c_pages.moveToNext()){
                Page page = getPage(c_pages.getInt(c_pages.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ID)));
                if(page != null)
                    pages.add(page);
            }
            c_pages.close();
            return pages;
            
		}
		c_pages.close();
		return null;
	        
		
	}
	
	public List<Page> getPagesByBinTypeId(int section_id,int bin_type_id){
		Cursor c_pages=this.database.query(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PAGE, new String[]{
				WasteInfoDBOpenHelper.COLUMN_ID,
				WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_DESCRIPTION,
				WasteInfoDBOpenHelper.COLUMN_PAGE_CONTENT,
				WasteInfoDBOpenHelper.COLUMN_PAGE_IMAGE,
				WasteInfoDBOpenHelper.COLUMN_SECTION_ID,
				WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR,
				WasteInfoDBOpenHelper.COLUMN_LATITUDE,
				WasteInfoDBOpenHelper.COLUMN_LONGITUDE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER,
				WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS,
				WasteInfoDBOpenHelper.COLUMN_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_NOT_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID,
				WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID
				
		
		}, WasteInfoDBOpenHelper.COLUMN_SECTION_ID+"="+section_id+" AND "+ WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS+"=1"+" AND "+ WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID+"="+bin_type_id, null,null,null, WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE+ " ASC");
		
		if(c_pages.getCount() != 0){
			List<Page> pages = new ArrayList<Page>();
			while(c_pages.moveToNext()){
				Page page = getPage(c_pages.getInt(c_pages.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ID)));
                if(page != null)
                    pages.add(page);
	
			}
			c_pages.close();
            return pages;
            
		}
		c_pages.close();
		return null;
	    
	}
	
	public List<Page> getPagesForCalendarPage(int section_id,int bin_type_id){
		Cursor c_pages=this.database.query(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PAGE, new String[]{
				WasteInfoDBOpenHelper.COLUMN_ID,
				WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_DESCRIPTION,
				WasteInfoDBOpenHelper.COLUMN_PAGE_CONTENT,
				WasteInfoDBOpenHelper.COLUMN_PAGE_IMAGE,
				WasteInfoDBOpenHelper.COLUMN_SECTION_ID,
				WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR,
				WasteInfoDBOpenHelper.COLUMN_LATITUDE,
				WasteInfoDBOpenHelper.COLUMN_LONGITUDE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER,
				WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS,
				WasteInfoDBOpenHelper.COLUMN_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_NOT_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID,
				WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID
				
		
		}, WasteInfoDBOpenHelper.COLUMN_SECTION_ID+"="+section_id+" AND "+ WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS+"=1"+ " AND " + WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID+"=0", null,null,null, WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE+ " DESC");
		
		if(c_pages.getCount() != 0){
			List<Page> pages = new ArrayList<Page>();
			while(c_pages.moveToNext()){
				Page page = getPage(c_pages.getInt(c_pages.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ID)));
                if(page != null)
                    pages.add(page);
	
			}
			c_pages.close();
            return pages;
            
		}
		c_pages.close();
		return null;
	    
	}
	public List<Page> getPagesForCalendarPage1(int section_id,int calendar_type){
		Cursor c_pages=this.database.query(com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PAGE, new String[]{
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_ID,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_PAGE_DESCRIPTION,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_PAGE_CONTENT,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_PAGE_IMAGE,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_SECTION_ID,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_LATITUDE,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_LONGITUDE,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_ALLOWED,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_NOT_ALLOWED,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID,
				com.wasteinfo.begavalley.bega.db.WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID
				
		
		},
//		WasteInfoDBOpenHelper.COLUMN_SECTION_ID+"="+section_id+" AND "+WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS+"=1"+" AND "+WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID+"="+bin_type_id+ " AND " +WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID+"=0", null,null,null,WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE+ " DESC");
		WasteInfoDBOpenHelper.COLUMN_SECTION_ID+"="+section_id+" AND "+ WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS+"=1"+" AND "+ WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR+"="+calendar_type,null,  null,null,null, WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE+ " ASC");
		if(c_pages.getCount() != 0){
			List<Page> pages = new ArrayList<Page>();
			while(c_pages.moveToNext()){
				Page page = getPage(c_pages.getInt(c_pages.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ID)));
                if(page != null)
                    pages.add(page);
	
			}
			c_pages.close();
            return pages;
            
		}
		c_pages.close();
		return null;
	    
	}
	
	public List<Page> getPages(int section_id){
		Cursor c_pages=this.database.query(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PAGE, new String[]{
				WasteInfoDBOpenHelper.COLUMN_ID,
				WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_DESCRIPTION,
				WasteInfoDBOpenHelper.COLUMN_PAGE_CONTENT,
				WasteInfoDBOpenHelper.COLUMN_PAGE_IMAGE,
				WasteInfoDBOpenHelper.COLUMN_SECTION_ID,
				WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR,
				WasteInfoDBOpenHelper.COLUMN_LATITUDE,
				WasteInfoDBOpenHelper.COLUMN_LONGITUDE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER,
				WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS,
				WasteInfoDBOpenHelper.COLUMN_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_NOT_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID,
				WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID
				
		
		}, WasteInfoDBOpenHelper.COLUMN_SECTION_ID+"="+section_id+" AND "+ WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS+"=1", null,null,null, WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE+ " ASC");
		
		if(c_pages.getCount() != 0){
			List<Page> pages = new ArrayList<Page>();
			while(c_pages.moveToNext()){
				Page page = getPage(c_pages.getInt(c_pages.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ID)));
                if(page != null)
                    pages.add(page);
	
			}
			c_pages.close();
            return pages;
            
		}
		c_pages.close();
		return null;
	    
	}
	
	public Page getPageByBinTypeIdAndBinColorId(int section_id,int bin_type_id,int bin_color_id){
		Cursor c_page = this.database.query(WasteInfoDBOpenHelper.TABLE_NAME_WASTE_INFO_PAGE, new String[] {
				WasteInfoDBOpenHelper.COLUMN_ID,
				WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_DESCRIPTION,
				WasteInfoDBOpenHelper.COLUMN_PAGE_CONTENT,
				WasteInfoDBOpenHelper.COLUMN_PAGE_IMAGE,
				WasteInfoDBOpenHelper.COLUMN_SECTION_ID,
				WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR,
				WasteInfoDBOpenHelper.COLUMN_LATITUDE,
				WasteInfoDBOpenHelper.COLUMN_LONGITUDE,
				WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER,
				WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS,
				WasteInfoDBOpenHelper.COLUMN_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_NOT_ALLOWED,
				WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID,
				WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID
				}
				, WasteInfoDBOpenHelper.COLUMN_SECTION_ID+"="+section_id+" AND "+ WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID+"="+bin_type_id+" AND "+ WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID +"="+bin_color_id+" AND "+WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS+"="+1,null,null,null,null);
		
		if(c_page.getCount() != 0){
			c_page.moveToNext();
			Page page=new Page();
			page.setId(c_page.getLong(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ID)));
			page.setPageTitle(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_TITLE)));
			page.setPageDescription(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_DESCRIPTION)));
			page.setPageContent(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_CONTENT)));
			page.setPageImage(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_IMAGE)));
			page.setSectionId(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_SECTION_ID)));
			page.setTypeOfCalendar(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_TYPE_OF_CALENDAR)));
			page.setLatitude(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_LATITUDE)));
			page.setLongitude(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_LONGITUDE)));
			page.setOrder(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_ORDER)));
			page.setPageStatus(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_PAGE_STATUS)));
			page.setAllowed(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_ALLOWED)));
			page.setNotAllowed(c_page.getString(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_NOT_ALLOWED)));
			page.setBinTypeId(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_BIN_TYPE_ID)));
			page.setBinColorId(c_page.getInt(c_page.getColumnIndex(WasteInfoDBOpenHelper.COLUMN_BIN_COLOR_ID)));
			c_page.close();
			return page;
		}
		c_page.close();
		return null;
		
	}
	
	
		
	
}
