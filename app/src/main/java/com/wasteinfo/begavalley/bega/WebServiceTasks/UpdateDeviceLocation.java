package com.wasteinfo.begavalley.bega.WebServiceTasks;

import android.content.Context;
import android.util.Log;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitAPI.MoiraAPI;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Test on 7/1/2016.
 */
public class UpdateDeviceLocation {
     private Context context;
    private String url;

    public UpdateDeviceLocation(Context context){

        this.context = context;
        this.url = context.getResources().getString(R.string.url);
    }

    public void UpdateDevice(String deviceid,String street,String suburb, String device_type){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MoiraAPI moiraAPI = retrofit.create(MoiraAPI.class);
        Call<String> updateDevice = moiraAPI.updateDevice(deviceid,street,suburb,device_type);
        updateDevice.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("Device Registration", "Registered Successfully");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                    t.printStackTrace();
            }
        });

    }

    public void UpdateDevice(String deviceid,String token,String device_type){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MoiraAPI moiraAPI = retrofit.create(MoiraAPI.class);
        Call<String> updateDevice = moiraAPI.registerDevice(deviceid,token,device_type);
        updateDevice.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("Device Registration", "Registered Successfully");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
}
