package com.wasteinfo.begavalley.bega.WebServiceTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.Fragments.CalendarFragment;
import com.wasteinfo.begavalley.bega.RetrofitAPI.MoiraAPI;
import com.wasteinfo.begavalley.bega.RetrofitModel.Location;
import com.wasteinfo.begavalley.bega.RetrofitModel.LocationResult;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.db.LocationDataSource;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Test on 6/24/2016.
 */
public class GetLocation {

    private LocationDataSource locationDataSource;
    private String url;
    private AddressActivity activity;
    private Context context;
    private SharedPref pref;
    private Location location;
//    private LocationResult locationResult;
    private ArrayList<LocationResult> locresult;
    private String device_uid;

    public GetLocation(Context context){
        this.context = context;
        this.url = context.getResources().getString(R.string.url);
        this.locationDataSource = new LocationDataSource(this.context);
        this.pref = new SharedPref(this.context);
        this.location = new Location();
        activity = (AddressActivity) context;
//        this.locationResult = new LocationResult();
    }


    public void getLocationMethod(final String suburb, String street, final AutoCompleteTextView base_suburb){
        final ProgressDialog pd = ProgressDialog.show(this.context, "", "Loading....", true, true);
        final String API = this.url;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MoiraAPI moiraAPI = retrofit.create(MoiraAPI.class);

        Call<Location> suburbsCall = moiraAPI.getLocations(suburb,street);

        suburbsCall.enqueue(new Callback<Location>() {
            @Override
            public void onResponse(Call<Location> call, Response<Location> response) {

                locresult = new ArrayList<LocationResult>();
                LocationResult locationResult;
                for (int i = 0; i < response.body().getResults().size(); i++) {
                    locationResult=new LocationResult();
                    locationResult.setItemId(i);
                    locationResult.setUnitNumber(response.body().getResults().get(i).getUnitNumber());
                    locationResult.setHouseNumber(response.body().getResults().get(i).getHouseNumber());
                    locationResult.setStreet(response.body().getResults().get(i).getStreet());
                    locationResult.setSuburb(response.body().getResults().get(i).getSuburb());
                    locationResult.setPostcode(response.body().getResults().get(i).getPostcode());
                    locationResult.setCollectionDay(response.body().getResults().get(i).getCollectionDay());
                    locationResult.setAtlWeek(response.body().getResults().get(i).getAtlWeek());
                    locationResult.setPropertyP(response.body().getResults().get(i).getPropertyP());
                    locationResult.setPfi(response.body().getResults().get(i).getPfi());
                    locationResult.setBinType(response.body().getResults().get(i).getBinType());
                    locationResult.setDateTime(response.body().getResults().get(i).getDateTime());
                    locationResult.setCurrentWeekType(response.body().getResults().get(i).getCurrentWeekType());
                    locationResult.setBinColors(response.body().getResults().get(i).getBinColors());
                    locationResult.setCollectionFrequencies(response.body().getResults().get(i).getCollectionFrequencies());
                    locationResult.setArea(response.body().getResults().get(i).getArea());
                    locationResult.setItemGarbage(response.body().getResults().get(i).getItemGarbage());
                    pref.setKeyValues("item_garbage",response.body().getResults().get(i).getItemGarbage());
                    locationResult.setItemOrganics(response.body().getResults().get(i).getItemOrganics());
                    pref.setKeyValues("item_organic",response.body().getResults().get(i).getItemOrganics());
                    locationResult.setItemRecycling(response.body().getResults().get(i).getItemRecycling());
                    pref.setKeyValues("item_recycling",response.body().getResults().get(i).getItemRecycling());

                    locresult.add(locationResult);


                }pd.dismiss();
                if (base_suburb!=null) {
                    base_suburb.setText("");
                }
                int size = locresult.size();
                if (locresult.size()==1){
                    OpenCalendar(locresult);
                }
                else if (locresult.size()==0) {
                    ShowMessage();
                }
                else{
                    showDialogChoice();
                }
            }



            @Override
            public void onFailure(Call<Location> call, Throwable t) {

            }
        });
    }

    private void showDialogChoice() {
        DialogChoiceAddress dialogObj = new DialogChoiceAddress(this.context, locresult);
        dialogObj.show();
    }

    private void ShowMessage() {

        Toast.makeText(context,"Please enter correct address",Toast.LENGTH_SHORT).show();
    }

    private void OpenCalendar(ArrayList<LocationResult> locatin) {
        LocationResult address = locatin.get(0);
        locationDataSource.open();
        locationDataSource.insertLocation(address);
        locationDataSource.close();
        pref.setKeyValues("item_id", (int) address.getItemId());
        pref.setKeyValues("street_name", address.getStreet());
        //pref.putInt("bin_type_id", address.getBinType());
        pref.setKeyValues("house_number", address.getHouseNumber());
        pref.setKeyValues("collection_day", address.getCollectionDay());
        pref.setKeyValues("collection_week", address.getCollectionDay());
        pref.setKeyValues("collection_frequencies", address.getCollectionFrequencies());
        pref.setKeyValues("week_type_match", address.getAtlWeek());
        pref.setKeyValues("item_garbage", address.getItemGarbage());
        pref.setKeyValues("item_organic", address.getItemOrganics());
        pref.setKeyValues("item_recycling", address.getItemRecycling());
        pref.setKeyValues("bin_colors", address.getBinColors());
        pref.setKeyValues("suburb", address.getSuburb());
        pref.setKeyValues("Area", address.getArea());
        pref.setKeyValues("date_time", address.getDateTime());
        CalendarFragment calendarFragment = new CalendarFragment();
        Bundle args = new Bundle();
        args.putInt("section_id", 3);
        calendarFragment.setArguments(args);
        activity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentID, calendarFragment).commit();

        device_uid = pref.getStringValues("device_uuid");
        UpdateDeviceLocation updateDeviceLocation = new UpdateDeviceLocation(this.context);
        updateDeviceLocation.UpdateDevice(device_uid, address.getStreet(), address.getSuburb(), "2");



    }
}
