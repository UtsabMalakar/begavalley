package com.wasteinfo.begavalley.bega.HelperClasses;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ListView;

import com.loopj.android.http.HttpGet;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.impl.client.BasicResponseHandler;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;


public class AsyncTaskGoogleApi2 extends AsyncTask<Double, Void, String> {

	private Context context;
	private String url;
	private ProgressDialog pDialog;
	private static final String LOGTAG = "WasteInfoVal";
	private ListView list;
	private Page page;
	
	public AsyncTaskGoogleApi2(Context context,String url,ListView list,Page page){
		this.context = context;
		this.url = url;
		this.list = list;
		this.page=page;
	}
	
	@Override
	protected String doInBackground(Double... params) {
		String finalUrl=this.url+"latlng="+params[1]+","+params[0];
		try
        {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(finalUrl);
            ResponseHandler<String> resHandler = new BasicResponseHandler();
            String response = httpClient.execute(httpGet, resHandler);
            Log.d(LOGTAG,"Response="+response);
            return response;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return "zero";  

        }
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		
		Log.d(LOGTAG,"Response="+result);
		try{
			JSONObject jObject = new JSONObject(result);
			
			JSONArray jsonMainNode = jObject.optJSONArray("results");
			
			String formatted_address=jsonMainNode.getJSONObject(0).getString("formatted_address").toString();
			Log.d(LOGTAG,"Formatted Address="+formatted_address);
			
			SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.context); 
			SharedPreferences.Editor editor = pref.edit();
			editor.putString("Destination_formatted_address",formatted_address);
			editor.commit();
			
			
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		// Dismiss the progress dialog
//		if (pDialog.isShowing())
//			pDialog.dismiss();

		
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this.context);
		String formatted_source_address = pref.getString("Source_formatted_address","");
		String formatted_destination_address = pref.getString("Destination_formatted_address","");
		
		AsyncTaskGoogleApi3 runner = new AsyncTaskGoogleApi3(context,"https://maps.googleapis.com/maps/api/directions/json?",page);
		runner.execute(formatted_source_address,formatted_destination_address);
	}

}
