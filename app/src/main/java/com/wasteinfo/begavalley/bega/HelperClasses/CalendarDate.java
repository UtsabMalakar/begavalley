package com.wasteinfo.begavalley.bega.HelperClasses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CalendarDate implements Serializable{
	private Calendar date;
    private ArrayList<Object> attachment;

    public CalendarDate() {
        this.date = null;
        this.attachment = new ArrayList<Object>();
    }

    public CalendarDate(Calendar date) {
        this.date = date;
        this.attachment = new ArrayList<Object>();
    }

    public CalendarDate(Calendar date, ArrayList<Object> attachment) {
        this.date = date;
        this.attachment = attachment;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public List<Object> getAttachment() {
        return attachment;
    }

    public void setAttachment(ArrayList<Object> attachment) {
        this.attachment = attachment;
    }

    public boolean isCurrentDate(){
    	Calendar rightNow = Calendar.getInstance();
        if(this.date.equals(rightNow))
            return true;
        return false;
    }

    @Override
	public String toString() {
		
		return "Data [Date=" + date +"]";
	}
    
}
