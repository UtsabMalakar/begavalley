package com.wasteinfo.begavalley.bega.RetrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Test on 6/24/2016.
 */
public class UpdateModel {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("date_time")
    @Expose
    private String date_time;
    @SerializedName("section")
    @Expose
    private List<Sections> sections = new ArrayList<Sections>();
    @SerializedName("page")
    @Expose
    private List<Page> page = new ArrayList<Page>();
    @SerializedName("problem_type")
    @Expose
    private List<ProblemType> problemTypes = new ArrayList<ProblemType>();


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public List<Sections> getSections() {
        return sections;
    }

    public void setSections(List<Sections> sections) {
        this.sections = sections;
    }

    public List<Page> getPage() {
        return page;
    }

    public void setPage(List<Page> page) {
        this.page = page;
    }

    public List<ProblemType> getProblemTypes() {
        return problemTypes;
    }

    public void setProblemTypes(List<ProblemType> problemTypes) {
        this.problemTypes = problemTypes;
    }
}
