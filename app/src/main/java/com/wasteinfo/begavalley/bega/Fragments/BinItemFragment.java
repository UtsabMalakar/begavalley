package com.wasteinfo.begavalley.bega.Fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wasteinfo.begavalley.bega.Activities.AddressActivity;
import com.wasteinfo.begavalley.bega.Activities.ExternalLinkActivity;
import com.wasteinfo.begavalley.bega.Alarm.Library;
import com.wasteinfo.begavalley.bega.HelperClasses.AsyncTaskGoogleApi3;
import com.wasteinfo.begavalley.bega.HelperClasses.MyLocation;
import com.wasteinfo.begavalley.R;
import com.wasteinfo.begavalley.bega.RetrofitModel.Page;
import com.wasteinfo.begavalley.bega.RetrofitModel.Sections;
import com.wasteinfo.begavalley.bega.SharedPref.SharedPref;
import com.wasteinfo.begavalley.bega.db.PageDataSource;
import com.wasteinfo.begavalley.bega.db.SectionDataSource;

import java.util.ArrayList;


public class BinItemFragment extends Fragment {


    private static final String LOGTAG = "WasteInfoVal";

    private long item_id;
    private int bin_type_id;
    private String bin_colors;


    private String head;
    private String image;
    private String footer_image;
    private String footer_image1;
    private String footer;
    private String baseUrl;

    private Library library;
    private int section_id;
    private long page_id;
    private WebView web;
    private Sections section;
    private Page page;
    private Context context;
    private GoogleMap map;
    private TextView page_title;
    private TextView heading;

    private SectionDataSource sectionDataSource;
    private PageDataSource pageDataSource;

    private ArrayList<String> contentArray;
    private ArrayList<String> titleArray;
    private ArrayList<String> latitudeArray;
    private ArrayList<String> longitudeArray;
    private ArrayList<Page> pageArray;
    private AddressActivity addressActivity;
    private SharedPref pref;


    public BinItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.addressActivity = (AddressActivity) context;
        this.context = context;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootview = inflater.inflate(R.layout.fragment_bin_item, container, false);
        // Inflate the layout for this fragment
//        this.context = getActivity().getApplicationContext();

        this.web = (WebView) rootview.findViewById(R.id.bin_item_webView);
        //this.web.setWebViewClient(new WebViewClient());
        this.map = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.bin_item_google_map)).getMap();

//        transaction.add(rootview.findViewById(R.id.rl_map_container),map).commit();
        this.sectionDataSource = new SectionDataSource(this.context);
        this.pageDataSource = new PageDataSource(this.context);
        this.pref = new SharedPref(this.context);
        this.library = new Library(this.context);

        this.page_title = (TextView) rootview.findViewById(R.id.bin_page_title);

        this.item_id = pref.getIntValues("item_id");
        this.bin_type_id = 3;
        this.bin_colors = pref.getStringValues("bin_colors");

        this.contentArray = new ArrayList<String>();
        this.titleArray = new ArrayList<String>();
        this.latitudeArray = new ArrayList<String>();
        this.longitudeArray = new ArrayList<String>();
        this.pageArray = new ArrayList<Page>();

        int section_id = getArguments().getInt("section_id");
        int bin_color_id = getArguments().getInt("bin_color");
//        int page_id1 = (int) page_id;

        int flag = pref.getIntValues("dbFlag");

        if (flag == 0) {
            if (this.library.isConnectingToInternet()) {
                //get page by calling web service
//                AsyncTaskPage asyncTaskPage = new AsyncTaskPage(this.context, this.web, this.map);
//                asyncTaskPage.execute(page_id1);
//                asyncTaskPage.delegate = this;
            } else {
//                Toast toast = Toast.makeText(this, "No internet connection!", Toast.LENGTH_SHORT);
//                toast.show();
            }

        }else{
//            this.sectionDataSource.open();
//            this.section = this.sectionDataSource.getSection(1);
//            this.sectionDataSource.close();
            this.pageDataSource.open();
            this.page = this.pageDataSource.getPageByBinTypeIdAndBinColorId(section_id,3,bin_color_id);
            this.pageDataSource.close();

            if (this.page==null){
                Toast.makeText(this.context, "No Calendar Page", Toast.LENGTH_SHORT).show();
                CalendarFragment calendarFragment = new CalendarFragment();
                Bundle args = new Bundle();
                args.putInt("section_id", 3);
                calendarFragment.setArguments(args);
                getActivity().getSupportFragmentManager().beginTransaction().
                        setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                        .replace(R.id.contentID, calendarFragment).commit();
            }
            else{
            this.page_title.setText(this.page.getPageTitle());
            String result=this.page.getAllowed()+"<br/>"+this.page.getNotAllowed();

//		result=result.replace("<strong>", "<h1>");
//		result=result.replace("</strong>", "</h1>");

            // append the style in the head
            head="<html><style>@font-face {font-family: 'exo-light'; src: url('fonts/Exo_Light.TTF');}</style><body>";

            if(bin_color_id == 1){
                image="<center><img src='file:///android_asset/images/dustbin-1.jpg' style='width:149; height:200'/></center>";
//				this.dustin_iv.setImageResource(R.drawable.dustbin_red);
            }else if(bin_color_id == 2){
                image="<center><img src='file:///android_asset/images/dustbin-2.jpg' style='width:149; height:200'/></center>";
//				this.dustin_iv.setImageResource(R.drawable.dustbin_green);
            }else{
                image="<center><img src='file:///android_asset/images/dustbin-3.jpeg' style='width:149; height:200'/></center>";
                //this.dustin_iv.setImageResource(R.drawable.dustbin_yellow);
            }



            footer="</body></html>";
            if(bin_color_id == 1){
                footer_image="<center><img src='file:///android_asset/images/dustbin_red.png' style='margin-bottom:10; width:149; height:200'/></center>";
//				this.dustin_iv.setImageResource(R.drawable.dustbin_red);
            }else if(bin_color_id == 2){
                footer_image="<center><img src='file:///android_asset/images/dustbin_green.png' style='width:149; height:200'/></center>";
                footer_image1="<center><img src='file:///android_asset/images/dustbin_green2.png' style='margin-bottom:10; margin-top:15; width:149; height:150'/></center>";
//				this.dustin_iv.setImageResource(R.drawable.dustbin_green);
            }else{
                footer_image="<center><img src='file:///android_asset/images/dustbin_yellow.png' /></center>";
                //this.dustin_iv.setImageResource(R.drawable.dustbin_yellow);
            }

            String finalHtmlString;
            if (bin_color_id == 2) {
                finalHtmlString=head + image + result + footer_image + footer_image1+footer;
            }
            else if(bin_color_id == 1){
                finalHtmlString=head + image + result + footer_image +footer;
            }
            else{
                finalHtmlString=head + image + result +footer;
            }
            //String finalHtmlString=head + result + footer;

            this.contentArray.add(finalHtmlString);

            this.titleArray.add(this.page.getPageTitle());



            baseUrl = "file:///android_asset/";

            try {
                this.web.loadDataWithBaseURL(baseUrl, finalHtmlString, "text/html", "UTF-8", null);
                System.out.println("INside try");

                //this.web.loadData(page.getPageContent(), "text/html", "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
//            if (this.page.getLatitude().isEmpty() && this.page.getLongitude().isEmpty()) {
//
//                rootview.findViewById(R.id.waste_item_google_map).setVisibility(View.GONE);
//
//            }
            if (this.page.getLatitude().isEmpty() && this.page.getLongitude().isEmpty()) {

                rootview.findViewById(R.id.bin_item_google_map).setVisibility(View.GONE);
                System.out.println("INside gone");

            } else {
                System.out.println("INside visible");
                LatLng position = new LatLng(Double.parseDouble(this.page.getLatitude()), Double.parseDouble(this.page.getLongitude()));
                MarkerOptions marker = new MarkerOptions();
                marker.position(position);
                marker.draggable(false);
                // marker.title(this.item.getMap_address());
                this.map.moveCamera(CameraUpdateFactory.zoomTo(14));
                this.map.moveCamera(CameraUpdateFactory.newLatLng(position));
                this.map.addMarker(marker);
                this.map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

                    @Override
                    public boolean onMarkerClick(Marker arg0) {
                        Log.d(LOGTAG, "marker is clicked!");
                        final ProgressDialog pd = ProgressDialog.show(context, "", "Loading...", true);
                        MyLocation.LocationsResult locationResult = new MyLocation.LocationsResult(){
                            @Override
                            public void gotLocation(Location location) {
                                //Got the location!
                                if (location != null) {
                                    //get latitude and longitude of the location
                                    double lng = location.getLongitude();
                                    double lat = location.getLatitude();

//							    	double lng=151.640;
//								    double lat=-30.493;

                                    String source_latlng = lat + "," + lng;
                                    String destination_latlng = page.getLatitude() + "," + page.getLongitude();
                                    AsyncTaskGoogleApi3 runner = new AsyncTaskGoogleApi3(context, "https://maps.googleapis.com/maps/api/directions/json?", page);
                                    runner.execute(source_latlng, destination_latlng);

//								     AsyncTaskGoogleApi1 runner = new AsyncTaskGoogleApi1(context,"https://maps.googleapis.com/maps/api/geocode/json?",page,lat,lng);
//								     runner.execute(lng,lat);
                                    //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                                } else {
                                    Toast toast = Toast.makeText(context,
                                            "You need to first set the location.", Toast.LENGTH_SHORT);
                                    toast.show();
                                }
                                pd.dismiss();
                            }
                        };
                        MyLocation myLocation = new MyLocation();
                        myLocation.getLocation(context, locationResult);
                        if (!myLocation.gps_enabled && !myLocation.network_enabled) {
                            System.out.println("INside map");
                            pd.dismiss();
                            // Build the alert dialog
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Location Services Not Active");
                            builder.setMessage("Please enable Location Services and GPS");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // Show location settings when the user acknowledges the alert dialog
                                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(intent);
                                }
                            });
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            Dialog alertDialog = builder.create();
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.show();
//								showMessage();
                        }
                        return true;
                    }

                });
            }

            this.web.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    System.out.println("INside webview");

                    Log.d(LOGTAG, "URL===" + url);

                    if (url.contains("localhost")) {
                        Log.d(LOGTAG, "Internal link clicked!");


                        String[] vals = url.split("//localhost/");
                        int pageId = Integer.parseInt(vals[1]);
                        Log.d(LOGTAG, "PageId===" + pageId);

                        pageDataSource.open();
                        page = pageDataSource.getPage(pageId);
                        pageDataSource.close();
                        page_title.setText(page.getPageTitle());

                        String finalHtmlString = head + page.getPageContent() + footer;
                        contentArray.add(finalHtmlString);
                        titleArray.add(page.getPageTitle());
                        pageArray.add(page);
                        if (!page.getLatitude().isEmpty() && !page.getLongitude().isEmpty()) {
                            latitudeArray.add(page.getLatitude());
                            longitudeArray.add(page.getLongitude());
                        } else {
                            latitudeArray.add("");
                            longitudeArray.add("");
                        }

                        // Display map or not
                        if (page.getLatitude().isEmpty()
                                && page.getLongitude().isEmpty()) {

                            rootview.findViewById(R.id.waste_item_google_map).setVisibility(View.GONE);
                            System.out.println("INside gone2");

                        } else {
                            System.out.println("INside visible2");
                            rootview.findViewById(R.id.waste_item_google_map).setVisibility(View.VISIBLE);

                            LatLng position = new LatLng(Double.parseDouble(page.getLatitude()), Double.parseDouble(page.getLongitude()));
                            MarkerOptions marker = new MarkerOptions();
                            marker.position(position);
                            marker.draggable(false);
                            // marker.title(this.item.getMap_address());
                            map.moveCamera(CameraUpdateFactory.zoomTo(14));
                            map.moveCamera(CameraUpdateFactory.newLatLng(position));
                            map.addMarker(marker);

                            map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

                                @Override
                                public boolean onMarkerClick(Marker arg0) {
                                    Log.d(LOGTAG, "marker is clicked!");
                                    final ProgressDialog pd = ProgressDialog.show(context, "", "Loading...", true);
                                    MyLocation.LocationsResult locationResult = new MyLocation.LocationsResult() {
                                        @Override
                                        public void gotLocation(Location location) {
                                            //Got the location!
                                            if (location != null) {
                                                //get latitude and longitude of the location
                                                double lng = location.getLongitude();
                                                double lat = location.getLatitude();


                                                String source_latlng = lat + "," + lng;
                                                String destination_latlng = page.getLatitude() + "," + page.getLongitude();
                                                AsyncTaskGoogleApi3 runner = new AsyncTaskGoogleApi3(context, "https://maps.googleapis.com/maps/api/directions/json?", page);
                                                runner.execute(source_latlng, destination_latlng);

//											     AsyncTaskGoogleApi1 runner = new AsyncTaskGoogleApi1(context,"https://maps.googleapis.com/maps/api/geocode/json?",page,lat,lng);
//											     runner.execute(lng,lat);
                                                //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                                            } else {
                                                Toast toast = Toast.makeText(context,
                                                        "You need to first set the location.", Toast.LENGTH_SHORT);
                                                toast.show();
                                            }
                                            pd.dismiss();
                                        }
                                    };
                                    MyLocation myLocation = new MyLocation();
                                    myLocation.getLocation(context, locationResult);

                                    if (!myLocation.gps_enabled && !myLocation.network_enabled) {
                                        pd.dismiss();
                                        // Build the alert dialog
                                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                        builder.setTitle("Location Services Not Active");
                                        builder.setMessage("Please enable Location Services and GPS");
                                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                // Show location settings when the user acknowledges the alert dialog
                                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                                startActivity(intent);
                                            }
                                        });
                                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                        Dialog alertDialog = builder.create();
                                        alertDialog.setCanceledOnTouchOutside(false);
                                        alertDialog.show();
//											showMessage();
                                    }
                                    return true;
                                }

                            });
                        }


                        try {
                            Log.d(LOGTAG, "Internal link clicked!");
                            web.loadDataWithBaseURL(baseUrl, finalHtmlString, "text/html", "UTF-8", null);

                            //web.loadData(page1.getPageContent(), "text/html", "UTF-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (url.startsWith("tel:")) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                        startActivity(intent);

                    } else if (url.startsWith("mailto:")) {
                        String[] vals = url.split(":");
                        String emailId = vals[1];
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setType("plain/text");
                        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailId});

                        startActivity(Intent.createChooser(emailIntent, "Send your email via:"));
                    } else if (url.contains("section")) {
                        String[] vals = url.split("//section/");
                        int sectionId = Integer.parseInt(vals[1]);
                        Material materialFragment = new Material();
                        Bundle args = new Bundle();
                        args.putLong("section_id",2);
                        materialFragment.setArguments(args);
                        addressActivity.getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                                .replace(R.id.contentID, materialFragment).commit();

                    } else if (url.contains("maps")) {
                        final String urlString = url;
                        final ProgressDialog pd = ProgressDialog.show(context, "", "Loading...", true);

                        MyLocation.LocationsResult locationResult = new MyLocation.LocationsResult() {
                            @Override
                            public void gotLocation(Location location) {
                                //Got the location!
                                if (location != null) {
                                    //get latitude and longitude of the location
                                    double lng = location.getLongitude();
                                    double lat = location.getLatitude();
                                    if (urlString.contains("saddr")) {
                                        String url = urlString.replace("saddr=srclatitude,srclongitude", "saddr=" + lat + "," + lng);

                                        Intent intent_external_link = new Intent(context, ExternalLinkActivity.class);
                                        intent_external_link.putExtra("external_url", url);
                                        startActivity(intent_external_link);
                                    } else {

                                        String source_latlng = lat + "," + lng;
                                        String destination_latlng = page.getLatitude() + "," + page.getLongitude();
                                        AsyncTaskGoogleApi3 runner = new AsyncTaskGoogleApi3(context, "https://maps.googleapis.com/maps/api/directions/json?", page);
                                        runner.execute(source_latlng, destination_latlng);
//			                        	AsyncTaskGoogleApi1 runner = new AsyncTaskGoogleApi1(context,"https://maps.googleapis.com/maps/api/geocode/json?",page,lat,lng);
//									    runner.execute(lng,lat);
                                        //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    }
                                } else {
                                    Toast toast = Toast.makeText(context,
                                            "You need to first set the location.", Toast.LENGTH_SHORT);
                                    toast.show();
                                }
                                pd.dismiss();
                            }
                        };
                        MyLocation myLocation = new MyLocation();
                        myLocation.getLocation(context, locationResult);

                        if (!myLocation.gps_enabled && !myLocation.network_enabled) {
                            pd.dismiss();
                            // Build the alert dialog
                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle("Location Services Not Active");
                            builder.setMessage("Please enable Location Services and GPS");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // Show location settings when the user acknowledges the alert dialog
                                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(intent);
                                }
                            });
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            Dialog alertDialog = builder.create();
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.show();
//							showMessage();
                        }


                    } else {
                        Log.d(LOGTAG, "External link clicked!");
                        Intent intent_external_link = new Intent(context, ExternalLinkActivity.class);
                        intent_external_link.putExtra("external_url", url);
                        startActivity(intent_external_link);
                        addressActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }

                    return true;
                }
            });
        }}



        return rootview;
    }

}
